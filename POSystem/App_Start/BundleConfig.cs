﻿using System.Web;
using System.Web.Optimization;

namespace POSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/clientCTRL").Include(
                      "~/Scripts/angularjs/controller/client.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/clientService").Include(
                      "~/Scripts/angularjs/service/clientService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/clientPOCtrl").Include(
                      "~/Scripts/angularjs/controller/clientPO.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/clientPOService").Include(
                      "~/Scripts/angularjs/service/clientPOService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/vendorCTRL").Include(
                     "~/Scripts/angularjs/controller/vendor.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/vendorService").Include(
                      "~/Scripts/angularjs/service/vendorService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/OrderPOCtrl").Include(
                    "~/Scripts/angularjs/controller/orderPO.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/OrderPOService").Include(
                      "~/Scripts/angularjs/service/orderPOService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/InvoiceCtrl").Include(
                    "~/Scripts/angularjs/controller/invoice.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/InvoiceService").Include(
                      "~/Scripts/angularjs/service/invoiceService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/LoginCtrl").Include(
                   "~/Scripts/angularjs/controller/login.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/LoginService").Include(
                      "~/Scripts/angularjs/service/loginService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/UserCtrl").Include(
                   "~/Scripts/angularjs/controller/user.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/UserService").Include(
                      "~/Scripts/angularjs/service/userService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/DashboardCtrl").Include(
                   "~/Scripts/angularjs/controller/dashboard.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/DashboardService").Include(
                      "~/Scripts/angularjs/service/dashboardService.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular/ReportCtrl").Include(
                   "~/Scripts/angularjs/controller/report.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular/ReportService").Include(
                      "~/Scripts/angularjs/service/reportService.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/po-management.css"));
        }
    }
}
