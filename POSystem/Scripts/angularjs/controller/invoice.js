﻿app.controller("InvoiceCtrl", ['$scope', 'InvoiceService', '$filter', function ($scope, InvoiceService, $filter) {

    $scope.searchInvoice = {};
    $scope.init = function () {
        InvoiceService.getInvoiceOrders().then(function (result) {
            $scope.InvoiceOrders = result.data.orders;
            angular.forEach($scope.InvoiceOrders, function (item) {
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");

                angular.forEach(item.vendor_po_detail, function (detail) {
                    if (detail.rate)
                        detail.rate = parseFloat(detail.rate).toFixed(2);
                });
            });
            $scope.clients = angular.copy(result.data.clients);
            $scope.clients.unshift({ id: '', name: '--Select--' });
            $scope.vendors = angular.copy(result.data.vendors);
            $scope.vendors.unshift({ id: '', name: '--Select--' });
            $scope.searchInvoice.client_id = $scope.clients[0].id;
            $scope.searchInvoice.prefered_vendor_id = $scope.vendors[0].id;
        });
    }

    $scope.init();

    $scope.editInvoice = function (invoice, order) {
        $scope.lastOrder = order;
        $scope.editInvoiceObj = {};
        $scope.editInvoiceObj = { invoice: angular.copy(invoice), order: angular.copy(order) };
        $("#edit-invoice").modal("show");
        $scope.submitted = false;
        if ($scope.editInvoiceObj.invoice.invoice_remarks == null || $scope.editInvoiceObj.invoice.invoice_remarks.length == 0) {
            $scope.editInvoiceObj.invoice.invoice_remarks = [];
            $scope.editInvoiceObj.invoice.invoice_remarks.push({ supplied_quantity: 0, invoice_number: '', remarks: '' });
        }
    }

    $scope.showRemarks = function (remarks) {
        $scope.viewRemarks = remarks;
        $("#view-remarks").modal("show");
    }

    $scope.updateInvoice = function (invoice, order) {
        if ($scope.formOrder.$valid) {
            var originalOrder = order;

            InvoiceService.updateInvoice(invoice).then(function (result) {
                $("#edit-invoice").modal("hide");
                switch (result.data.status_id) {
                    case 2:
                        invoice.statusname = "Partial";
                        result.data.statusname = "Partial";
                        break;
                    case 3:
                        invoice.statusname = "Completed";
                        result.data.statusname = "Completed";
                        break;
                    case 4:
                        invoice.statusname = "Ordered";
                        result.data.statusname = "Ordered";
                        break;
                }
                var count = 0;
                for (var i = 0; i < $scope.lastOrder.vendor_po_detail.length; i++) {
                    if ($scope.lastOrder.vendor_po_detail[i].Id == result.data.Id)
                        break;
                    else
                        count++;
                }

                $scope.lastOrder.vendor_po_detail[count] = result.data;
                $scope.chkIsCompleted($scope.lastOrder);

            });
        }
        else
            $scope.submitted = true;
    }

    $scope.searchInvoice = function () {
        $scope.clientSearch = angular.copy($scope.searchInvoice.client_id);
        $scope.vendorSearch = angular.copy($scope.searchInvoice.prefered_vendor_id);
        $scope.poNumberSearch = angular.copy($scope.searchInvoice.ponumber);
    }

    $scope.chkIsCompleted = function (order) {

        var orders = order.vendor_po_detail;
        var count = 0;
        for (var i = 0; i < orders.length; i++) {
            if (orders[i].statusname == "Completed")
                count++;
        }
        if (orders.length == count)
            order.statusname = "Completed";
        else
            order.statusname = "Partial";
    }

    $scope.RemoveCompleted = function () {
        var orders = $scope.InvoiceOrders;
        var deletedStr = "";
        var notDeletedOrders = [];
        for (var i = 0; i < orders.length; i++) {
            if (orders[i].statusname == "Completed")
                deletedStr += orders[i].id + ",";
            else
                notDeletedOrders.push(orders[i]);
        }

        if (deletedStr)
            InvoiceService.removeCompleted(deletedStr.substring(0, deletedStr.length - 1)).then(function (result) {
                $scope.InvoiceOrders = notDeletedOrders;
            });
    }

    $scope.addNewInvoice = function (invoice_remarks) {
        invoice_remarks.push({ supplied_quantity: 0, invoice_number: '', remarks: '' });
    }

    $scope.markComplete = function (order) {
        InvoiceService.markComplete(order.id).then(function (result) {
            order.statusname = "Completed";
            angular.forEach(order.vendor_po_detail, function (item) {
                item.statusname = "Completed";
            });
        });
    }
}]);
function isNumber(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46 || charCode == 8)
        return true;
    else
        return false;
}