﻿app.controller("OrderPOCtrl", ['$scope', 'OrderPOService', '$filter', function ($scope, OrderPOService, $filter) {

    $scope.details = $scope.details1 = [];
    $scope.searchOrderPO = {};
    $scope.OrderPO = {};

    $scope.paperTypes = [{ "id": 1, "name": 'Kraft' }, { "id": 2, "name": 'Duplex' }];
    $scope.searchOrderPO.paperTypeId = 1;
    $scope.init = function () {
        OrderPOService.getOtherDetails().then(function (result) {
            $scope.clients = result.data.clients;
            $scope.clients.unshift({ id: '', name: '--Select--' });
            $scope.searchOrderPO.clientId = $scope.clients[0].id;
            $scope.vendors = result.data.vendors;
            $scope.vendors.unshift({ id: '', name: '--Select--' });
            $scope.OrderPO.clientId = $scope.clients[0].id;
            $scope.rateTypes = angular.copy(result.data.rateTypes);
            $scope.unitTypes = angular.copy(result.data.unitTypes);
            $scope.freightTypes = angular.copy(result.data.freightTypes);
            $scope.searchOrderPO.po_number = "";
        })
    }

    $scope.getAllPO_Numbers = function () {
        OrderPOService.getAllPO_Numbers($scope.searchOrderPO.clientId).then(function (result) {
            $scope.searchOrderPO.po_number = "";
            $scope.searchOrderPO.paperTypeId = 1;
            $scope.details1 = [];
            $scope.details = [];
            $scope.po_numbers = result.data;
            if (result.data && result.data.length > 0) {
                $scope.searchOrderPO.po_number = $scope.po_numbers[0];
            }

        });
    }
    $scope.addOrder = function () {
        if ($scope.formOrder.$valid) {
            $scope.OrderPO.client_id = $scope.searchOrderPO.clientId;
            $scope.OrderPO.client_po_number = $scope.searchOrderPO.po_number;
            //var result = $scope.clients.filter(function (obj) {
            //    return obj.id == $scope.OrderPO.client_id;
            //});
            //$scope.OrderPO.Tin_No = result[0].tin_number;
            $scope.OrderPO.freightName = $("#Freight option:selected").text();
            $scope.OrderPO.PaperType = $scope.searchOrderPO.paperTypeId;
            $scope.loader = true;
            $scope.OrderPO.vendor_po_detail = [];
            angular.forEach($scope.details1, function (item) {
                var delivery_date = item.delivery_date.toString().split('/');
                item.delivery_date = $filter('date')(new Date(delivery_date[2], delivery_date[1] - 1, delivery_date[0]), "MM/dd/yyyy");
            })
            
            $scope.OrderPO.vendor_po_detail = angular.copy($scope.details1);
            OrderPOService.createOrder($scope.OrderPO).then(function (result) {
                for (var i = 0; i < $scope.OrderPO.vendor_po_detail.length; i++) {
                    $scope.OrderPO.vendor_po_detail[i].delivery_date = $filter('date')(new Date($scope.OrderPO.vendor_po_detail[i].delivery_date), "dd/MM/yyyy");
                    if ($scope.OrderPO.vendor_po_detail[i].quantity_left != $scope.OrderPO.vendor_po_detail[i].ordered_quantity) {
                        $scope.OrderPO.vendor_po_detail[i].quantity_left = $scope.OrderPO.vendor_po_detail[i].quantity_left - $scope.OrderPO.vendor_po_detail[i].ordered_quantity;
                        $scope.OrderPO.vendor_po_detail[i].ordered_quantity = 0;
                        if ($scope.OrderPO.vendor_po_detail[i].quantity_left && parseInt($scope.OrderPO.vendor_po_detail[i].quantity_left) > 0)
                            $scope.details.push($scope.OrderPO.vendor_po_detail[i]);
                    }
                }
                
                $scope.details1 = [];                
                $scope.loader = false;
                $("#client-form").modal("hide");

            });
        }
        else
            $scope.submitted = true;
    }

    $scope.showOrder = function () {
        
        //$scope.checkLimits($scope.details1);
        $scope.limitsTrue = true;

        if ($scope.limitsTrue == true) {
            $scope.OrderPO = {};
            $scope.formOrder.$setPristine();
            $scope.OrderPO.vendor_id = $scope.vendors[0].id;
            $scope.OrderPO.freight_type = $scope.freightTypes[0].id;

            $scope.OrderPO.Buyer = angular.copy($scope.clients[0]);
            $scope.OrderPO.Consignee = angular.copy($scope.clients[0]);
            $scope.OrderPO.Delivery = angular.copy($scope.clients[0]);

            if ($scope.searchOrderPO.paperTypeId == 2)
            {
                $scope.OrderPO.vendor_id = $scope.searchOrderPO.vendor_id;
                $scope.changeVendor($scope.OrderPO);
            }
        }
        else
            event.stopPropagation();

    }

    $scope.closeOrder = function () {
        $("#client-form").modal("hide");

    }

    $scope.typeName = function (id, collection, typeId) {
        var types = collection;
        for (var i = 0; i < types.length; i++) {
            if (types[i].id == id) {
                if (typeId == 1)
                    return types[i].rate_type1;
                else if (typeId == 2)
                    return types[i].unit_type1;
                else
                    return types[i].name;

            }
        }
    }

    $scope.init();

    $scope.changeVendor = function (OrderPO) {
        var obj = { id: OrderPO.vendor_id };
        var index = getArrayIndex($scope.vendors, obj);
        OrderPO.vendorEmail = $scope.vendors[index].email;
        OrderPO.vendor = $scope.vendors[index];
    }

    $scope.checkLimits = function (detailsObj) {
        $scope.limitsTrue = true;
        for (var i = 0; i < detailsObj.length; i++) {
            if (detailsObj[i].ordered_quantity < 0 || detailsObj[i].ordered_quantity > detailsObj[i].quantity_left) {
                $scope.limitsTrue = false;
                break;
            }           
        }      
    }

    $scope.showVendors = function () {
        $scope.details1 = [];
        $scope.details = [];
        $scope.searchOrderPO.vendor_id = $scope.vendors[0].id;

    }

    $scope.searchOrder = function () {
        if ($scope.po_numbers && $scope.po_numbers.length > 0) {
            if ($scope.searchOrderPO.paperTypeId == 1)
                $scope.searchOrderPO.vendor_id = 0;
            if ($scope.formSearchOrder.$valid) {

                OrderPOService.getSearchOrders($scope.searchOrderPO).then(function (result) {
                    $scope.details1 = [];
                    $scope.details = result.data;
                    angular.forEach($scope.details, function (item) {
                        if (item.delivery_date)
                            item.delivery_date = $filter('date')(item.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");

                        if (item.rate)
                            item.rate = parseFloat(item.rate).toFixed(2);

                    });
                });
            }
            else
                $scope.searchSubmit = true;
        }
    }

    var getArrayIndex = function (arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === obj.id) {
                return i;
            }
        }
    }

}]);
function isNumber(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46 || charCode == 8)
        return true;
    else
        return false;
}