﻿app.controller("ReportCtrl", ['$scope', 'ReportService', '$filter', function ($scope, ReportService, $filter) {
    $scope.Report = {};
    $scope.Types = [{ "id": 1, "name": 'Client PO' }, { "id": 2, "name": 'Vendor PO' }];

    $scope.Report.type = $scope.Types[0].id;

    $scope.init = function () {
        ReportService.getData().then(function (result) {
            $scope.clients = angular.copy(result.data.clients);
            $scope.clients.unshift({ id: '', name: 'All' });
            $scope.vendors = angular.copy(result.data.vendors);
            $scope.vendors.unshift({ id: '', name: 'All' });
            $scope.Report.client_id = $scope.clients[0].id;
            $scope.Report.vendorId = $scope.vendors[0].id;
        });
    }

    $scope.search = function () {
        var fromDate = $("#datetimepicker2").val().toString().split('/');
        $scope.Report.fromDate = $filter('date')(new Date(fromDate[2], fromDate[1] - 1, fromDate[0]), "MM/dd/yyyy");

        var toDate = $("#datetimepicker3").val().toString().split('/');
        $scope.Report.toDate = $filter('date')(new Date(toDate[2], toDate[1] - 1, toDate[0]), "MM/dd/yyyy");

        ReportService.search($scope.Report).then(function (result) {
            $scope.searchReports = result.data;
            angular.forEach($scope.searchReports, function (item) {
                if (item.rate)
                    item.rate = parseFloat(item.rate).toFixed(2);
            });
        });
    }

    $scope.init();

}]);
function onlyBackspace(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 8)
        return true;
    else
        return false;
}