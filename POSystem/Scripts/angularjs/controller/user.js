﻿app.controller("UserCtrl", ['$scope', 'UserService', function ($scope, UserService) {

    $scope.chkList = [{ name: "PO", isChecked: false }, { name: "Order", isChecked: false }, { name: "Invoice", isChecked: false }, { name: "Report", isChecked: false }, { name: "UM", isChecked: false }];
    $scope.UpdatedList = [];
    $scope.init = function () {
        UserService.getUsers().then(function (result) {
            $scope.users = result.data;
        });
    }
    $scope.init();
    $scope.emailpattern = /^[a-zA-Z]+[a-z0-9._]+@[a-z0-9]+\.[a-z.]{2,5}$/;

    $scope.registerUser = function () {
        if ($scope.formRegister.$valid) {
            $scope.register.Claims = $scope.UpdatedList;
            UserService.register($scope.register).then(function (result) {
                $scope.ResetForm();
                $scope.init();
                $("#client-form").modal("hide");

            });
        }
        else
            $scope.submitted = true;
    }

    $scope.editUser = function (user) {
        $scope.register = angular.copy(user);
        var claims = $scope.register.Claims;
        for (var i = 0; i < claims.length; i++) {

            switch (claims[i].ClaimType) {
                case "PO":
                    $scope.chkList[0].isChecked = true;
                    $scope.UpdatedList.push({ "ClaimType": "PO", "ClaimValue": 1 });
                    break;
                case "Order":
                    $scope.chkList[1].isChecked = true;
                    $scope.UpdatedList.push({ "ClaimType": "Order", "ClaimValue": 1 });
                    break;
                case "Invoice":
                    $scope.chkList[2].isChecked = true;
                    $scope.UpdatedList.push({ "ClaimType": "Invoice", "ClaimValue": 1 });
                    break;
                case "Report":
                    $scope.chkList[3].isChecked = true;
                    $scope.UpdatedList.push({ "ClaimType": "Report", "ClaimValue": 1 });
                    break;
                case "UM":
                    $scope.chkList[4].isChecked = true;
                    $scope.UpdatedList.push({ "ClaimType": "UM", "ClaimValue": 1 });
                    break;
            }

        }
        $("#client-form").modal("show");
    }

    $scope.deleteUser = function (user) {
        UserService.deleteUser(user).then(function (result) {
            $scope.init();
        });
    }

    $scope.updateCheckVal = function (type) {
        if (type.isChecked == false) {
            for (var i = 0; i < $scope.UpdatedList.length; i++) {
                if ($scope.UpdatedList[i].ClaimType == type.name)
                    $scope.UpdatedList.splice(i, 1);
            }
        }
        else
            $scope.UpdatedList.push({ "ClaimType": type.name, "ClaimValue": 1 });
    }

    $scope.showUser = function () {
        $scope.ResetForm();
    }

    $scope.closeForm = function () {
        $("#client-form").modal("hide");
    }

    $scope.showResetForm = function (user) {
        $scope.reset = { Email: user.Email };
        $scope.formReset.$setPristine();
        $scope.submitted = false;
        $scope.resetPasswordError = false;

    }

    $scope.ResetForm = function () {
        $scope.UpdatedList = [];
        $scope.register = { Id: 0 };
        $scope.formRegister.$setPristine();
        $scope.submitted = false;

    }

    $scope.searchUser = function () {
        $scope.userNameSearch = angular.copy($scope.searchUserObj.UserName);
        $scope.emailSearch = angular.copy($scope.searchUserObj.Email);
    }

    $scope.resetPassword = function () {
        if ($scope.formReset.$valid) {
            UserService.resetPassword($scope.reset).then(function (result) {
                if (result.data == false)
                    $scope.resetPasswordError = true;
                else
                    $("#reset").modal("hide");
            });
        }
        else
            $scope.submitted = true;
    }
}]);

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

app.directive("compareTo", compareTo);