﻿app.controller("DashboardCtrl", ['$scope', 'DashboardService', 'clientPOService', 'OrderPOService', '$filter', function ($scope, DashboardService, clientPOService, OrderPOService, $filter) {

    $scope.dashboard = {};

    $scope.init = function () {
        DashboardService.getData().then(function (result) {

            $scope.dashboard = result.data;

            $scope.rateTypes = angular.copy(result.data.rateTypes);
            $scope.unitTypes = angular.copy(result.data.unitTypes);
            $scope.SizeType = [{ "id": "1", "name": 'inch' }, { "id": "2", "name": 'cm' }];

            angular.forEach($scope.dashboard.rejectedPOs, function (item) {
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");

                if (item.client_po_detail) {
                    angular.forEach(item.client_po_detail, function (item) {
                        if (item.delivery_date)
                            item.delivery_date = $filter('date')(item.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                        if (item.rate)
                            item.rate = parseFloat(item.rate).toFixed(2);

                        $scope.checkChange(item);
                    });
                }
            })
            angular.forEach($scope.dashboard.rejectedOrders, function (item) {
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                angular.forEach(item.vendor_po_detail, function (item) {
                    if (item.delivery_date)
                        item.delivery_date = $filter('date')(item.delivery_date.replace('/Date(', '').replace(')/', ''), "MM/dd/yyyy");

                });
            })
            angular.forEach($scope.dashboard.approvedOrders, function (item) {
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
            })
            angular.forEach($scope.dashboard.approvedPOs, function (item) {
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                if (item.client_po_detail) {
                    angular.forEach(item.client_po_detail, function (item) {
                        if (item.delivery_date)
                            item.delivery_date = $filter('date')(item.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                        if (item.rate)
                            item.rate = parseFloat(item.rate).toFixed(2);
                        $scope.checkChange(item);
                    });
                }



            })
            angular.forEach($scope.dashboard.pendingDeliveries, function (item1) {
                if (item1.delivery_date)
                    item1.delivery_date = $filter('date')(item1.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
            })
        });
    }
    $scope.typeName = function (id, collection, typeId) {
        var types = collection;
        for (var i = 0; i < types.length; i++) {
            if (types[i].id == id) {
                if (typeId == 1)
                    return types[i].rate_type1;
                else if (typeId == 2)
                    return types[i].unit_type1;
                else
                    return types[i].name;

            }
        }
    }
    $scope.addUpdateDetail = function (index, detail) {
        if (detail.id > 0)
            detail.isEqual = angular.equals($scope.lastClientPODetail, detail);

        detail.isDashboard = true;
        var lastDate = angular.copy(detail.delivery_date);
        var delivery_date = detail.delivery_date.toString().split('/');
        detail.delivery_date = $filter('date')(new Date(delivery_date[2], delivery_date[1] - 1, delivery_date[0]), "MM/dd/yyyy");
        clientPOService.addUpdateDetail(detail).then(function (result) {
            detail.isHide = false;
            detail.rate = parseFloat(detail.rate).toFixed(2);
            detail.delivery_date = lastDate;
        });
    }

    $scope.editOrder = function (order) {
        order.status_id = 1;
    }

    $scope.addUpdateOrder = function (index, order) {
        OrderPOService.updateRejectedOrder(order).then(function (result) {
            order.status_id = 0;
        });
    }

    $scope.completeRejectedOrder = function (order, index) {
        OrderPOService.completeRejectedOrder(order).then(function (result) {
            $scope.dashboard.rejectedOrders.splice(index, 1);
        });
    }
    $scope.completeRejectedPO = function (order, index) {
        clientPOService.completeRejectedOrder(order).then(function (result) {
            $scope.dashboard.rejectedPOs.splice(index, 1);
        });
    }

    $scope.deleteRejectedOrder = function (details, order,index) {
        OrderPOService.deleteRejectedOrder(details).then(function (result) {
            order.vendor_po_detail.splice(index, 1);
        });
    }

    $scope.deleteOrder = function (order) {
        order.status_id = 1;

    }

    $scope.editDetail = function (index, type, papertype) {
        type.isHide = true;
        type.PaperType = papertype;
        $scope.lastClientPODetail = angular.copy(type);
        $scope.parentIndex = index;
    }

    $scope.deleteDetail = function (type, PODetails) {
        clientPOService.deleteDetail(type).then(function (result) {
            var index = getArrayIndex(PODetails, type);
            PODetails.splice(index, 1);
        });
    }

    var getArrayIndex = function (arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === obj.id) {
                return i;
            }
        }
    }

    $scope.checkChange = function (type) {
        //if ($scope.clientPO.paper_type == 2) {
        var length = type.paper_length, breadth = type.paper_breadth;
        if (!type.paper_length)
            length = 0;
        if (!type.paper_breadth)
            breadth = 0;
        if (parseInt(length) > parseInt(breadth))
            type.grainVal = 'Short'
        else if (parseInt(length) == parseInt(breadth))
            type.grainVal = 'Mix'
        else
            type.grainVal = 'Long';
        //}
    }
    $scope.init();

}]);
app.directive('customzdatetime', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                autoclose: true
            }).on('change', function (e) {
                ngModelCtrl.$setViewValue(e.target.value);
                scope.$apply();
            });
        }
    };
});
function isNumber(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46 || charCode == 8)
        return true;
    else
        return false;
}