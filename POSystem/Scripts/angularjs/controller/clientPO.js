﻿app.controller("clientPOCtrl", ['$scope', 'clientPOService', '$filter', function ($scope, clientPOService, $filter) {


    $scope.clientPO = {};
    $scope.clientPOs = [];
    $scope.paperTypes = [{ "id": 1, "name": 'Kraft' }, { "id": 2, "name": 'Duplex' }];

    $scope.SizeType = [{ "id": "1", "name": 'inch' }, { "id": "2", "name": 'cm' }];
    $scope.addClientPO = function () {

        if ($scope.clientPO.id > 0) {
            //$scope.clientPO.isEqual = angular.equals($scope.lastClientPO, $scope.clientPO);
            $scope.clientPO.isEqual = true;
            angular.forEach($scope.clientPO.client_po_detail, function (item, index_) {
                item.isEqual = angular.equals($scope.lastClientPO.client_po_detail[index_], item);
                if (item.isEqual == false)
                    $scope.clientPO.isEqual = false;
            });
        }
        else
            $scope.clientPO.isEqual = true;

        if ($scope.formClientPO.$valid) {
            $scope.loader = true;

            var date_received = $scope.clientPO.date_received.toString().split('/');
            $scope.clientPO.date_received = $filter('date')(new Date(date_received[2], date_received[1] - 1, date_received[0]), "MM/dd/yyyy");
            angular.forEach($scope.clientPO.client_po_detail, function (item) {
                var delivery_date = item.delivery_date.toString().split('/');
                item.delivery_date = $filter('date')(new Date(delivery_date[2], delivery_date[1] - 1, delivery_date[0]), "MM/dd/yyyy");
                if (item.rate)
                    item.rate = parseFloat(item.rate).toFixed(2);
            });

            clientPOService.addClientPO($scope.clientPO).then(function (result) {
                if (result.data.isNotValid == false && result.data.isNotDeleted == false) {
                    if (result.data.date_received)
                        result.data.date_received = $filter('date')(result.data.date_received.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                    if (result.data.date_added)
                        result.data.date_added = $filter('date')(result.data.date_added.replace('/Date(', '').replace(')/', ''), "MM/dd/yyyy");
                    if (result.data.date_modified)
                        result.data.date_modified = $filter('date')(result.data.date_modified.replace('/Date(', '').replace(')/', ''), "MM/dd/yyyy");
                    if (result.data.client_po_detail) {
                        angular.forEach(result.data.client_po_detail, function (item) {
                            if (item.delivery_date)
                                item.delivery_date = $filter('date')(item.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                            if (item.rate)
                                item.rate = parseFloat(item.rate).toFixed(2);

                            $scope.checkChange(item);
                        });
                    }
                    var vendorObj = $scope.vendors.filter(function (obj) {
                        return obj.id == $scope.clientPO.prefered_vendor_id;
                    });
                    result.data.vendorName = vendorObj[0].name;
                    if ($scope.lastIndex > -1)
                        $scope.clientPOs[$scope.lastIndex] = result.data
                    else
                        $scope.clientPOs.unshift(result.data);

                    $scope.clientPO = {};
                    $scope.loader = false;
                    $scope.formClientPO.$setPristine();
                    $("#client-form").modal("hide");
                    $scope.submitted = false;
                }
                else if (result.data.isNotValid)
                    alert("Supplied quantity for one or more quality is less than already processed vendor POs");
                else if (result.data.isNotDeleted)
                    alert("Client PO details cannot be deleted as one or more vendor POs are in process");

                $scope.loader = false;
            });
        }
        else
            $scope.submitted = true;
    }

    $scope.addUpdateDetail = function (index, detail) {

        if (detail.id > 0)
            detail.isEqual = angular.equals($scope.lastClientPODetail, detail);

        var lastDate = angular.copy(detail.delivery_date);
        var delivery_date = detail.delivery_date.toString().split('/');
        detail.delivery_date = $filter('date')(new Date(delivery_date[2], delivery_date[1] - 1, delivery_date[0]), "MM/dd/yyyy");
        clientPOService.addUpdateDetail(detail).then(function (result) {
            if (result.data != false) {
                detail.delivery_date = lastDate;
                detail.isHide = false;
                detail.is_updated = result.data.is_updated;
                detail.rate = parseFloat(detail.rate).toFixed(2);
            }
            else
                alert("Supplied quantity is less than already processed vendor POs");
        });
    }

    $scope.deleteDetail = function (type, PODetails) {
        clientPOService.deleteDetail(type).then(function (result) {
            if (result.data == false)
                alert("Client PO detail cannot be deleted as one or more vendor POs are in process");
            else {
                var index = getArrayIndex(PODetails, type);
                PODetails.splice(index, 1);
            }
        });
    }

    $scope.typeName = function (id, collection, typeId) {
        var types = collection;
        for (var i = 0; i < types.length; i++) {
            if (types[i].id == id) {
                if (typeId == 1)
                    return types[i].rate_type1;
                else if (typeId == 2)
                    return types[i].unit_type1;
                else
                    return types[i].name;

            }
        }
    }

    $scope.init = function () {
        clientPOService.getClientPO().then(function (result) {
            $scope.clients = angular.copy(result.data.clients);
            $scope.clients.unshift({ id: '', name: '--Select--' });
            $scope.vendors = angular.copy(result.data.vendors);
            $scope.vendors.unshift({ id: '', name: '--Select--' });
            $scope.rateTypes = angular.copy(result.data.rateTypes);
            //$scope.rateTypes.unshift({ id: '', rate_type1: '--Select--' });
            $scope.unitTypes = angular.copy(result.data.unitTypes);
            //$scope.unitTypes.unshift({ id: '', unit_type1: '--Select--' });
            $scope.clientPOs = angular.copy(result.data.clientPOs);
            angular.forEach($scope.clientPOs, function (item) {
                if (item.date_received)
                    item.date_received = $filter('date')(item.date_received.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                if (item.date_added)
                    item.date_added = $filter('date')(item.date_added.replace('/Date(', '').replace(')/', ''), "MM/dd/yyyy");
                if (item.date_modified)
                    item.date_modified = $filter('date')(item.date_modified.replace('/Date(', '').replace(')/', ''), "MM/dd/yyyy");
                var isDeleteRequest_ = 0;
                angular.forEach(item.client_po_detail, function (item1) {
                    if (item1.delivery_date)
                        item1.delivery_date = $filter('date')(item1.delivery_date.replace('/Date(', '').replace(')/', ''), "dd/MM/yyyy");
                    if (item1.status_id == 9)
                        isDeleteRequest_++;
                    $scope.checkChange(item1);
                    if (item1.rate)
                        item1.rate = parseFloat(item1.rate).toFixed(2);
                });
                if (item.client_po_detail.length == isDeleteRequest_)
                    item.isPODeleteRequest = true;
            });
           
            $scope.searchPO.client_id = $scope.clientPO.client_id = $scope.clients[0].id;
            $scope.searchPO.prefered_vendor_id = $scope.clientPO.prefered_vendor_id = $scope.vendors[0].id;
        });
    }

    $scope.init();


    $scope.changeClient = function (clientPO) {
        var obj = { id: clientPO.client_id };
        var index = getArrayIndex($scope.clients, obj);
        clientPO.clientEmail = $scope.clients[index].email;
    }

    $scope.editClientPO = function (clientPO, event) {
        $scope.clientPO = angular.copy(clientPO);
        $scope.changeClient($scope.clientPO);
        if ($scope.clientPO.client_po_detail.length == 0)
            $scope.addNewRow();
        if (!$scope.clientPO.client_id)
            $scope.clientPO.client_id = $scope.clients[0].id;
        if (!$scope.clientPO.prefered_vendor_id)
            $scope.clientPO.prefered_vendor_id = $scope.vendors[0].id;
        $scope.Status = "Update";
        $scope.lastIndex = getArrayIndex($scope.clientPOs, clientPO);
        $scope.lastClientPO = angular.copy($scope.clientPO);
        $("#client-form").modal("show");
        event.stopPropagation();
    }

    $scope.searchPO = function () {
        $scope.clientSearchId = angular.copy($scope.searchPO.client_id);
        $scope.vendorSearchId = angular.copy($scope.searchPO.prefered_vendor_id);
        if (!$scope.vendorSearchId)
            $scope.vendorSearchId = undefined;
        $scope.poSearchNumber = angular.copy($scope.searchPO.po_number);
        $scope.poSearchDateReceived = $("#datetimepicker2").val();
    }

    $scope.editDetail = function (index, type, papertype) {
        type.isHide = true;
        type.PaperType = papertype;
        $scope.lastClientPODetail = angular.copy(type);
        $scope.parentIndex = index;
    }


    $scope.deleteClientPO = function (clientPO) {
        clientPOService.deleteClientPO(clientPO).then(function (result) {
            if (result.data == false)
                alert("Client PO detail cannot be deleted as one or more vendor POs are in process");
            else {
                clientPO.isPODeleteRequest = true;
                angular.forEach(clientPO.client_po_detail, function (item) {
                    item.is_updated = 1;
                })
                //var index = getArrayIndex($scope.clientPOs, clientPO);
                //$scope.clientPOs.splice(index, 1);
                $scope.formClientPO.$setPristine();
                $scope.clientPO = {};
            }
        });
    }

    $scope.showClient = function () {
        $scope.Status = "Save";
        $scope.clientPO = {};
        $scope.clientPO.client_po_detail = [];
        $scope.addNewRow();
        $scope.submitted = false;
        $scope.formClientPO.$setPristine();
        $scope.clientPO.paper_type = $scope.paperTypes[0].id;
        $scope.clientPO.client_id = $scope.clients[0].id;
        $scope.clientPO.prefered_vendor_id = $scope.vendors[0].id;
    }

    $scope.removeMember = function ($index, type) {
        //$scope.clientPO.client_po_detail.splice($index, 1);
        if (type.is_updated != true)
            type.IsDeleted = true;
        //if ($scope.clientPO.client_po_detail.length == 0)
        //    $scope.addNewRow();
    }
    $scope.close = function () {
        $("#client-form").modal("hide");
        angular.forEach($scope.clientPO.client_po_detail, function (item) {
            item.IsDeleted = false;
        });
    }

    $scope.addNewRow = function () {
        $scope.clientPO.client_po_detail.push({ "id": 0, "paper_length": '', "quality": '', "size": $scope.SizeType[0].id, "paper_breadth": '', "bf_grain": '', "gsm": '', "quantity": '', "unit": $scope.unitTypes[0].id, "rate": '', "rate_type_id": $scope.rateTypes[0].id, "delivery_date": '', "status_id": 1 });
    }

    $scope.changePaperType = function () {
        $scope.clientPO.client_po_detail = [];
        $scope.addNewRow();
    }

    var getArrayIndex = function (arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === obj.id) {
                return i;
            }
        }
    }

    $scope.checkChange = function (type) {
        //if ($scope.clientPO.paper_type == 2) {
        var length = type.paper_length, breadth = type.paper_breadth;
        if (!type.paper_length)
            length = 0;
        if (!type.paper_breadth)
            breadth = 0;
        if (parseInt(length) > parseInt(breadth))
            type.grainVal = 'Short'
        else if (parseInt(length) == parseInt(breadth))
            type.grainVal = 'Mix'
        else
            type.grainVal = 'Long';
        //}
    }

}]);
app.directive('customzdatetime', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                autoclose: true
            }).on('change', function (e) {
                ngModelCtrl.$setViewValue(e.target.value);
                scope.$apply();
            });
        }
    };
});
function isNumber(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46 || charCode == 8)
        return true;
    else
        return false;
}
function onlyBackspace(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 8)
        return true;
    else
        return false;
}
