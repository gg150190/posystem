﻿app.controller("VendorCtrl", ['$scope', 'vendorService', function ($scope, vendorService) {
    $scope.lastIndex = -1;
    $scope.submitted = false;
    $scope.vendors = [];
    $scope.Status = "Add Vendor";
    $scope.emailpattern = /^[a-zA-Z]+[a-z0-9._]+@[a-z0-9]+\.[a-z.]{2,5}$/;

    $scope.init = function () {
        vendorService.getVendors().then(function (result) {
            $scope.vendors = angular.copy(result.data.vendors);
            $scope.states = angular.copy(result.data.states);
        });
    }

    $scope.init();

    $scope.addVendor = function () {
        if ($scope.formVendor.$valid) {
            vendorService.addVendor($scope.vendor).then(function (result) {
                if ($scope.lastIndex > -1)
                    $scope.vendors[$scope.lastIndex] = $scope.vendor;
                else {
                    $scope.vendor.id = result.data.id;
                    $scope.vendors.unshift($scope.vendor);
                }
                $scope.formVendor.$setPristine();
                $scope.vendor = {};
                $("#vendor-form").modal("hide");
                $scope.submitted = false;
            });
        }
        else
            $scope.submitted = true;
    }

    $scope.editVendor = function (vendor) {
        $scope.vendor = angular.copy(vendor);
        $scope.lastIndex = getArrayIndex($scope.vendors, vendor);
        $scope.Status = "Update Vendor";
        $("#vendor-form").modal("show");
    }

    $scope.search = function () {
        $scope.vendorName = angular.copy($scope.search.name1);
        $scope.vendorCity = angular.copy($scope.search.city);
    }

    $scope.deleteVendor = function (vendor) {
        vendorService.deleteVendor(vendor).then(function (result) {
            var index = getArrayIndex($scope.vendors, vendor);
            $scope.vendors.splice(index, 1);
            $scope.formVendor.$setPristine();
            $scope.vendor = {};
        });
    }

    $scope.showVendor = function () {
        $scope.Status = "Add Vendor";
        $scope.vendor = {};
        $scope.vendor.state_id = $scope.states[0].id;
        $scope.submitted = false;
        $scope.formVendor.$setPristine();
    }

    $scope.close = function () {
        $("#vendor-form").modal("hide");
    }

    var getArrayIndex = function (arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === obj.id) {
                return i;
            }
        }
    }
}]);