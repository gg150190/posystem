﻿app.controller("LoginCtrl", ['$scope', 'LoginService', function ($scope, LoginService) {

    $scope.login = {};
    $scope.emailpattern = /^[a-zA-Z]+[a-z0-9._]+@[a-z0-9]+\.[a-z.]{2,5}$/;

    $scope.loginUser = function () {
        if ($scope.formLogin.$valid) {
            LoginService.loginUser($scope.login).then(function (result) {
                if (result.data.Success == false)
                    $scope.loginerror = true;
                if (result.data.Result.Id > 0) {
                    $scope.loginerror = false;
                    location.href = "/dashboard/Dashboard";
                }
            });
        }
        else
            $scope.submitted = true;
    }


}]);