﻿app.controller("ClientCtrl", ['$scope', 'clientService', function ($scope, clientService) {
    $scope.lastIndex = -1;
    $scope.clients = [];
    $scope.submitted = false;
    $scope.client = {};
    $scope.Status = "Add Client";

    $scope.emailpattern = /^[a-zA-Z]+[a-z0-9._]+@[a-z0-9]+\.[a-z.]{2,5}$/;


    $scope.init = function () {
        clientService.getClients().then(function (result) {
            $scope.clients = angular.copy(result.data.clients);
            $scope.states = angular.copy(result.data.states);
        });
    }

    $scope.init();

    $scope.addClient = function () {
        if ($scope.formClient.$valid) {
            clientService.addClient($scope.client).then(function (result) {
                if ($scope.lastIndex > -1)
                    $scope.clients[$scope.lastIndex] = $scope.client;
                else {
                    $scope.client.id = result.data.id;
                    $scope.clients.unshift($scope.client);
                }
                $scope.formClient.$setPristine();
                $scope.client = {};
                $("#client-form").modal("hide");
                $scope.submitted = false;
            });
        }
        else
            $scope.submitted = true;
    }

    $scope.editClient = function (client) {
        $scope.client = angular.copy(client);
        $scope.Status = "Update Client";
        $scope.lastIndex = getArrayIndex($scope.clients, client);
        $("#client-form").modal("show");
    }

    $scope.search = function () {
        $scope.clientName = angular.copy($scope.search.name1);
        $scope.clientCity = angular.copy($scope.search.city);
    }

    $scope.deleteClient = function (client) {
        clientService.deleteClient(client).then(function (result) {
            var index = getArrayIndex($scope.clients, client);
            $scope.clients.splice(index, 1);
            $scope.formClient.$setPristine();
            $scope.client = {};
        });
    }

    $scope.showClient = function () {
        $scope.Status = "Add Client";
        $scope.client = {};
        $scope.client.state_id = $scope.states[0].id;
        $scope.submitted = false;
        $scope.formClient.$setPristine();
    }

    $scope.close = function () {
        $("#client-form").modal("hide");
    }

    var getArrayIndex = function (arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === obj.id) {
                return i;
            }
        }
    }
}]);