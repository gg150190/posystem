﻿app.factory("OrderPOService", ['$http', function ($http) {

    var orderPO = {};

    orderPO.getOtherDetails = function () {
        return $http.get("/order/getorderdetails");
    }
    orderPO.getSearchOrders = function (searchOrderPO) {
        return $http.post("/order/getpoorderdetails/", JSON.stringify(searchOrderPO));
    }
    orderPO.createOrder = function (order) {
        return $http.post("/order/createorder", JSON.stringify(order));
    }
    orderPO.getAllPO_Numbers = function (clientId) {
        return $http.get("/order/getponumbers/" + clientId);

    }
    orderPO.updateRejectedOrder = function (detail) {
        return $http.post("/order/updaterejectedorder", JSON.stringify(detail));
    }
    orderPO.deleteRejectedOrder = function (detail) {
        return $http.post("/order/deleterejectedorder", JSON.stringify(detail));
    }
    orderPO.completeRejectedOrder = function (order) {
        return $http.post("/order/completerejectedorder", JSON.stringify(order));
    }

    return orderPO;

}]);