﻿app.factory("LoginService", ['$http', function ($http) {

    var login = {};
    login.loginUser = function (loginUser) {
        return $http.post("/account/login", JSON.stringify(loginUser));
    }

    return login;

}]);