﻿app.factory("vendorService", ['$http', function ($http) {

    var vendor = {};
    vendor.getVendors = function () {
        return $http.get("/vendor/get");
    };

    vendor.addVendor = function (vendor) {
        return $http.post("/vendor/add", JSON.stringify(vendor));
    };

    vendor.deleteVendor = function (vendor) {
        return $http.delete("/vendor/delete/" + parseInt(vendor.id));
    };


    return vendor;
}]);