﻿app.factory("UserService", ['$http', function ($http) {

    var user = {};
    user.register = function (register) {
        return $http.post("/account/register", JSON.stringify(register));
    },

    user.getUsers = function () {
        return $http.get("/user/users");
    },

    user.deleteUser = function (user) {
        return $http.delete("/account/DeleteUser/" + user.Id);
    },

    user.resetPassword = function (reset) {
        return $http.post("/account/ResetPassword", JSON.stringify(reset));
    }

    return user;

}]);