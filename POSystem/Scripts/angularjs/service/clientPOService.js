﻿app.factory("clientPOService", ['$http', function ($http) {

    var clientPO = {};

    clientPO.getClientPO = function () {
        return $http.get("/client/getpo");
    };

    clientPO.addClientPO = function (clientPO) {
        return $http.post("/client/addpo", JSON.stringify(clientPO));
    };

    clientPO.deleteClientPO = function (clientPO) {
        return $http.delete("/client/deletepo/" + clientPO.id);
    };

    clientPO.addUpdateDetail = function (detail) {
        return $http.post("/client/addupdatedetail/", JSON.stringify(detail));
    }

    clientPO.deleteDetail = function (detail) {
        return $http.delete("/client/deletepodetail/" + detail.id);
    }
    clientPO.completeRejectedOrder = function (order) {
        return $http.post("/client/completerejectedpo", JSON.stringify(order));
    }
    return clientPO;
}]);