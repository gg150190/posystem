﻿app.factory("ReportService", ['$http', function ($http) {

    var report = {};

    report.getData = function () {
        return $http.get("/report/getData");
    },
    report.search = function (data) {
        return $http.post("/report/search",JSON.stringify(data));
    }

    return report;

}]);