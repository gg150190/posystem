﻿app.factory("clientService", ['$http', function ($http) {

    var client = {};
    client.getClients = function () {
        return $http.get("/client/get");
    };

    client.addClient = function (client) {
        return $http.post("/client/add", JSON.stringify(client));
    };

    client.deleteClient = function (client) {
        return $http.delete("/client/delete/" + parseInt(client.id));
    };


    return client;
}]);