﻿app.factory("InvoiceService", ['$http', function ($http) {

    var invoice = {};

    invoice.getInvoiceOrders = function () {
        return $http.get("/invoice/getinvoice");
    }

    invoice.updateInvoice = function (invoice) {
        return $http.post("/invoice/updateinvoice", JSON.stringify(invoice));
    }

    invoice.removeCompleted = function (deletedStr) {
        return $http.delete("/invoice/remove/" + deletedStr);
    }

    invoice.markComplete = function (id) {
        return $http.get("/invoice/markcomplete/" + id);

    }

    return invoice;
}]);