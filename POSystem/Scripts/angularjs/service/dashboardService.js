﻿app.factory("DashboardService", ['$http', function ($http) {

    var dashboard = {};
    dashboard.getData = function () {
        return $http.get("/dashboard/data");
    }

    return dashboard;

}]);