﻿using POBusiness.Models;
using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSystem.Controllers
{
    [Authorize]
    [RoutePrefix("vendor")]
    public class VendorController : Controller
    {
        private IVendorRepository _repository;
        public VendorController(IVendorRepository repository)
        {
            _repository = repository;
        }

        // GET: Client
        public ActionResult Vendor()
        {
            return View();
        }

        [Route("get")]
        public JsonResult GetVendors()
        {
            Client_Vendor_State client = new Client_Vendor_State();
            client.vendors = _repository.All;
            client.states = _repository.GetStates();
            return Json(client, JsonRequestBehavior.AllowGet);
        }

        [Route("add")]
        [HttpPost]
        public JsonResult AddVendor(Vendor vendor)
        {
            if (vendor.id > 0)
                _repository.Update(vendor);
            else
                _repository.Add(vendor);
            return Json(vendor, JsonRequestBehavior.AllowGet);
        }

        [Route("delete/{id:int}")]
        [HttpDelete]
        public JsonResult DeleteVendor(int id)
        {
            _repository.Delete(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}