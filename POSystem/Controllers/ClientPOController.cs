﻿using POBusiness.Models;
using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Configuration;

namespace POSystem.Controllers
{
    [Authorize]
    [RoutePrefix("client")]
    public class ClientPOController : Controller
    {
        private IClientPORepository _repository;
        private IClientRepository _clientRepository;
        private IVendorRepository _vendorRepository;

        public ClientPOController(IClientPORepository repository, IClientRepository clientRepository, IVendorRepository vendorRepository)
        {
            _repository = repository;

            _clientRepository = clientRepository;
            _vendorRepository = vendorRepository;
        }

        [ClaimsAuthorization(ClaimType = "PO", ClaimValue = "1")]
        [Route("createpo")]
        public ActionResult ClientPO()
        {
            return View();
        }

        [Route("getpo")]
        public JsonResult GetClientPO()
        {
            Client_ClientPO clientPO = new Client_ClientPO();
            clientPO.clientPOs = new List<POBusiness.Models.ClientPO>();
            clientPO.clientPOs = _repository.All.OrderByDescending(t => t.date_modified).ToList();
            clientPO.clients = _clientRepository.All;
            clientPO.vendors = _vendorRepository.All;
            clientPO.rateTypes = _repository.AllRateTypes;
            clientPO.unitTypes = _repository.AllUnitTypes;
            return Json(clientPO, JsonRequestBehavior.AllowGet);
        }

        [Route("addpo")]
        [HttpPost]
        public JsonResult AddPO(ClientPO clientPO)
        {
            if (clientPO.id > 0)
            {
                if (clientPO.isEqual == false)
                {
                    clientPO.date_modified = DateTime.UtcNow;
                    clientPO.modified_by = User.Identity.GetUserId<int>();
                    _repository.Update(clientPO);
                }
            }
            else
            {
                clientPO.created_by = User.Identity.GetUserId<int>();
                clientPO.modified_by = User.Identity.GetUserId<int>();
                if (clientPO.prefered_vendor_id != null && clientPO.prefered_vendor_id > 0)
                {
                    clientPO.vendorName = _vendorRepository.Find(clientPO.prefered_vendor_id).name;
                }
                _repository.Add(clientPO);
                var context = new ApplicationDbContext();
                var allUsers = context.Users.Where(t => t.LockoutEndDateUtc == null).ToList();
                var filterList = allUsers.Where(t => t.Platform == "android" && t.Token != null && t.Token != "").Select(t => t.Token).ToList();
                PushNotification.PushAndroidNotification(filterList, ConfigurationManager.AppSettings["POMessage"]);
                PushNotification.PushAppleNotification(allUsers.Where(t => t.Platform == "ios" && t.Token != null && t.Token != "").Select(t => t.Token).ToList(), ConfigurationManager.AppSettings["POMessage"]);
            }
            return Json(clientPO, JsonRequestBehavior.AllowGet);
        }

        [Route("deletepo/{id:int}")]
        [HttpDelete]
        public JsonResult DeleteClientPO(int id)
        {
            var res = _repository.DeleteClientPO_(id);
            POUpdateNotification();
            return Json(res == true ? true : false, JsonRequestBehavior.AllowGet);
        }

        [Route("addupdatedetail")]
        [HttpPost]
        public JsonResult UpdatePODetail(ClientPODetail detail)
        {
            if (detail.isEqual == false)
            {
                if (detail.PaperType == 2)
                {
                    double result = 0;
                    if (detail.size == "1")
                        result = (Convert.ToDouble(detail.paper_length) * Convert.ToDouble(detail.paper_breadth) * Convert.ToDouble(detail.gsm) * 144 * 6.45) / 10000000;
                    else
                        result = (Convert.ToDouble(detail.paper_length) * Convert.ToDouble(detail.paper_breadth) * Convert.ToDouble(detail.gsm) * 144) / 10000000;

                    if (result > 32)
                        detail.rim_weight = Convert.ToString(result / 2);
                    else
                        detail.rim_weight = Convert.ToString(result);
                }
                bool status = _repository.AddUpdateClientPODetail(detail, true);
                if (detail.isDashboard == false)
                {
                    if (status == true)
                        POUpdateNotification();
                    else
                        return Json(false, JsonRequestBehavior.AllowGet);
                }
            }

            //if (detail.isEqual == false && _repository.AddUpdateClientPODetail(detail, true) == false)
            //    return Json(false, JsonRequestBehavior.AllowGet);
            //else
            return Json(detail, JsonRequestBehavior.AllowGet);

        }


        [Route("deletepodetail/{id:int}")]
        [HttpDelete]
        public JsonResult DeleteClientPODetail(int id)
        {
            var res = _repository.DeleteClientPODetail(id);
            POUpdateNotification();
            return Json(res == true ? true : false, JsonRequestBehavior.AllowGet);
        }

        [Route("completerejectedpo")]
        [HttpPost]
        public JsonResult completerejectedpo(ClientPO clientPO)
        {
            _repository.CompleteRejectedOrder(clientPO.id, 2);
            var context = new ApplicationDbContext();
            var allUsers = context.Users.Where(t => t.LockoutEndDateUtc == null).ToList();
            var filterList = allUsers.Where(t => t.Platform == "android" && t.Token != null && t.Token != "").Select(t => t.Token).ToList();
            PushNotification.PushAndroidNotification(filterList, ConfigurationManager.AppSettings["POMessage"]);
            PushNotification.PushAppleNotification(allUsers.Where(t => t.Platform == "ios" && t.Token != null && t.Token != "").Select(t => t.Token).ToList(), ConfigurationManager.AppSettings["POMessage"]);
            return Json(clientPO, JsonRequestBehavior.AllowGet);
        }

        public void POUpdateNotification()
        {
            var context = new ApplicationDbContext();
            var allUsers = context.Users.Where(t => t.LockoutEndDateUtc == null).ToList();
            var filterList = allUsers.Where(t => t.Platform == "android" && t.Token != null && t.Token != "").Select(t => t.Token).ToList();
            PushNotification.PushAndroidNotification(filterList, ConfigurationManager.AppSettings["POUpdateMessage"]);
            PushNotification.PushAppleNotification(allUsers.Where(t => t.Platform == "ios" && t.Token != null && t.Token != "").Select(t => t.Token).ToList(), ConfigurationManager.AppSettings["POUpdateMessage"]);
        }


    }
}