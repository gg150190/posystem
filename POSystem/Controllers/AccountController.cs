﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using POSystem.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using POBusiness.Models.ApiModel;

namespace POSystem.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _AppRoleManager;
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager AppRoleManager
        {
            get
            {
                return _AppRoleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                //return View(model);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var user = await UserManager.FindByNameAsync(model.Email);

            var loggedinUser = await UserManager.FindAsync(model.Email, model.Password);
            if (loggedinUser != null)
            {
                // Now user have entered correct username and password.
                // Time to change the security stamp
                await UserManager.UpdateSecurityStampAsync(loggedinUser.Id);
                loggedinUser.Token = model.Token;
                loggedinUser.Platform = model.Platform;
                UserManager.Update(loggedinUser);
            }



            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result1 = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result1)
            {
                case SignInStatus.Success:
                    Session["UserName"] = loggedinUser.Name;
                    //return RedirectToLocal(returnUrl);
                    return Json(new { Status = true, Result = loggedinUser, Code = 0, Message = "Success" }, JsonRequestBehavior.AllowGet);
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    return Json(new { Status = false, Result = (object)null, Code = user == null ? 2 : 1, Message = "Success" }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(RegisterViewModel model)
        {
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, FirstName = model.FirstName, LastName = model.LastName, Address = model.Address, PhoneNumber = model.PhoneNumber, Id = model.Id };
            if (user.Id == 0)
            {
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //await AssignRolesToUser(user.Id, new string[] { "Admin" });
                    //await AssignRolesToUser(user.Id, new string[] { model.Role });
                    AssignClaimsToUser(user.Id, model.Claims);
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //await this.UserManager.AddToRoleAsync(user.Id, model.UserRoles);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                UpdateUser(model);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        public void AssignClaimsToUser(int userId, List<ClaimBindingModel> claimsToAssign)
        {
            var appUser = this.UserManager.FindByIdAsync(userId).Result;
            foreach (var claim in appUser.Claims.ToList())
            {
                var res = this.UserManager.RemoveClaimAsync(userId, new Claim(claim.ClaimType, claim.ClaimValue)).Result;
            }
            if (claimsToAssign != null && claimsToAssign.Count() > 0)
            {
                foreach (ClaimBindingModel claimModel in claimsToAssign)
                {
                    //if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                    //{
                    //    await this.UserManager.RemoveClaimAsync(userId, new Claim(claimModel.Type, claimModel.Value));
                    //}
                    var res1 = this.UserManager.AddClaimAsync(userId, new Claim(claimModel.ClaimType, claimModel.ClaimValue)).Result;
                }
            }
        }

        public async Task<ActionResult> RemoveClaimsFromUser(int userId, List<ClaimBindingModel> claimsToRemove)
        {
            var appUser = await this.UserManager.FindByIdAsync(userId);
            foreach (ClaimBindingModel claimModel in claimsToRemove)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.ClaimType))
                {
                    await this.UserManager.RemoveClaimAsync(userId, new Claim(claimModel.ClaimType, claimModel.ClaimValue));
                }
            }
            return null;
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        public void UpdateUser(RegisterViewModel model)
        {
            var appUser = this.UserManager.FindByIdAsync(model.Id).Result;
            // Update it with the values from the view model
            appUser.Name = model.Name;
            appUser.FirstName = model.FirstName;
            appUser.LastName = model.LastName;
            appUser.PhoneNumber = model.PhoneNumber;
            appUser.Email = model.Email;
            appUser.Address = model.Address;
            //string resetToken = await UserManager.GeneratePasswordResetTokenAsync(appUser.Id);
            //IdentityResult passwordChangeResult = await UserManager.ResetPasswordAsync(appUser.Id, resetToken, model.Password);

            // Apply the changes if any to the db
            UserManager.Update(appUser);
            AssignClaimsToUser(model.Id, model.Claims);

        }

        [HttpDelete]
        [Route("account/DeleteUser/{Id:int}")]
        public async Task<JsonResult> DeleteUser(int Id)
        {
            var appUser = await this.UserManager.FindByIdAsync(Id);
            appUser.IsDelete = true;
            //string resetToken = await UserManager.GeneratePasswordResetTokenAsync(appUser.Id);
            //IdentityResult passwordChangeResult = await UserManager.ResetPasswordAsync(appUser.Id, resetToken, model.Password);

            // Apply the changes if any to the db
            //UserManager.Update(appUser);
            await UserManager.SetLockoutEnabledAsync(Id, true);
            await UserManager.SetLockoutEndDateAsync(Id, DateTime.Today.AddYears(10));
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> ResetPassword(ResetPasswordViewModel model)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            string resetToken = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var result = await UserManager.ResetPasswordAsync(user.Id, resetToken, model.Password);
            return Json(result.Succeeded, JsonRequestBehavior.AllowGet);

        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff(int userId=0)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            if (userId > 0)
            {
                //var userId = User.Identity.GetUserId<int>();
                var appUser = this.UserManager.FindByIdAsync(userId).Result;
                appUser.Token = "";
                UserManager.Update(appUser);
                object res = null;
                return Json(new
                {
                    Message = "Success",
                    Status = Convert.ToBoolean(ActionStatus.Successfull),
                    Result = res
                }
                , JsonRequestBehavior.AllowGet);
            }
            
            return RedirectToAction("Login", "Account");
        }
        [HttpPost]
        public JsonResult UpdateToken(UpdateTokenModel model)
        {
            var appUser = this.UserManager.FindByIdAsync(model.id).Result;
            appUser.Token = model.token;
            object res = null;
            UserManager.Update(appUser);
            return Json(new
            {
                Message = "Success",
                Status = Convert.ToBoolean(ActionStatus.Successfull),
                Result = res
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region UserRoles
        public async Task<ActionResult> GetRole(int Id)
        {
            var role = await this.AppRoleManager.FindByIdAsync(Id);

            if (role != null)
            {
                //  return Ok(TheModelFactory.Create(role));
            }

            //return NotFound();
            return null;

        }

        public ActionResult GetAllRoles()
        {
            var roles = this.AppRoleManager.Roles;

            //return Ok(roles);
            return null;

        }

        public async Task<ActionResult> Create(string name)
        {
            //if (!ModelState.IsValid)
            //{
            //return BadRequest(ModelState);
            //}

            var role = new CustomRole { Name = name };

            var result = await this.AppRoleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                //return GetErrorResult(result);
            }
            return null;
        }

        public async Task<ActionResult> DeleteRole(int Id)
        {
            var role = await this.AppRoleManager.FindByIdAsync(Id);

            if (role != null)
            {
                IdentityResult result = await this.AppRoleManager.DeleteAsync(role);

                if (!result.Succeeded)
                {
                    //     return GetErrorResult(result);
                }

                //   return Ok();
            }

            // return NotFound();
            return null;
        }

        public async Task<ActionResult> AssignRolesToUser(int userId, string[] rolesToAssign)
        {
            var appUser = await this.UserManager.FindByIdAsync(userId);

            if (appUser == null)
            {
                //return NotFound();
            }

            var currentRoles = await this.UserManager.GetRolesAsync(appUser.Id);

            var rolesNotExists = rolesToAssign.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();

            if (rolesNotExists.Count() > 0)
            {
                ModelState.AddModelError("", string.Format("Roles '{0}' does not exixts in the system", string.Join(",", rolesNotExists)));
                //return BadRequest(ModelState);
            }

            IdentityResult removeResult = await this.UserManager.RemoveFromRolesAsync(appUser.Id, currentRoles.ToArray());

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to remove user roles");
                //return BadRequest(ModelState);
            }

            IdentityResult addResult = await this.UserManager.AddToRolesAsync(appUser.Id, rolesToAssign);

            if (!addResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to add user roles");
                //return BadRequest(ModelState);
            }

            return null;
        }
        #endregion
    }
}