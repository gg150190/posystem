﻿using POBusiness.Models;
using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.IO;

namespace POSystem.Controllers
{
    [Authorize]
    [RoutePrefix("Order")]
    public class OrderController : Controller
    {
        private IClientPORepository _repository;
        private IClientRepository _clientRepository;
        private IVendorRepository _vendorRepository;
        static int incNumber = 1;
        static int lastMonth = DateTime.Now.Month;
        public OrderController(IClientPORepository repository, IClientRepository clientRepository, IVendorRepository vendorRepository)
        {
            _repository = repository;
            _clientRepository = clientRepository;
            _vendorRepository = vendorRepository;
        }

        [ClaimsAuthorization(ClaimType = "Order", ClaimValue = "1")]
        [Route("CreateOrder")]
        public ActionResult Index()
        {
            return View("CreateOrder");
        }

        [Route("getorderdetails")]
        public JsonResult GetOtherDetails()
        {
            OrderPO orderPO = new OrderPO();
            orderPO.clients = _clientRepository.All;
            orderPO.vendors = _vendorRepository.All;
            orderPO.rateTypes = _repository.AllRateTypes;
            orderPO.unitTypes = _repository.AllUnitTypes;
            orderPO.freightTypes = _repository.GetFreights;
            return Json(orderPO, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("getpoorderdetails")]
        public JsonResult GetOrderDetails(OrderSearch search)
        {
            var res = _repository.FindByClientId(search.clientId, search.paperTypeId, search.po_number, search.vendor_id);
            res = res.Where(t => t.quantity_left > 0).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [Route("getponumbers/{clientId:int}")]
        public JsonResult GetAllPONumbers(int clientId)
        {
            var res = _repository.GetAllPONumbers(clientId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("createorder")]
        public JsonResult CreateOrder(Order order)
        {
            try
            {
                order.buyerId = order.Buyer.id;
                order.consigneeId = order.Consignee.id;
                order.deliveryId = order.Delivery.id;
                int incNumber = 0;
                var path = System.Web.HttpContext.Current.Server.MapPath("/Content/value.txt");
                using (StreamReader sr = new StreamReader(path))
                {
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        incNumber = Convert.ToInt32(line.Trim());
                    }
                }
                if (lastMonth != DateTime.Now.Month)
                {
                    incNumber = 1;
                    lastMonth = DateTime.Now.Month;
                }
                order.po_number = DateTime.Now.Month.ToString("d2") + "/" + DateTime.Now.Year + "/" + String.Format("{0:0000}", incNumber);
                incNumber++;
                using (StreamWriter sw = new StreamWriter(path, false))
                {
                    sw.WriteLine(incNumber);
                }
                order.created_by = User.Identity.GetUserId<int>();
                order.modified_by = User.Identity.GetUserId<int>();
                var orderId = _repository.CreateOrder(order);
                order.id = orderId;
                CreateExcel_Notifications(order);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                string createText = ex.Message + "from2";
                System.IO.File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/Logger.txt"), createText);
                return null;
            }
        }
        [HttpPost]
        [Route("updaterejectedorder")]
        public JsonResult UpdateRejectedOrder(OrderDetail detail)
        {
            _repository.UpdateRejectedOrder(detail);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("deleterejectedorder")]
        public JsonResult DeleteRejectedOrder(OrderDetail detail)
        {
            _repository.DeleteRejectedOrder(detail);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("completerejectedorder")]
        public JsonResult CompleteRejectedOrder(Order order)
        {
            _repository.CompleteRejectedOrder(order.id, 1);
            CreateExcel_Notifications(order);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public void CreateExcel_Notifications(Order order)
        {
            ExcelModel model = new ExcelModel();
            if (order.PaperType == 2)
            {
                model.CreateExcel(order, 1);
                model.CreateExcel(order, 2);
            }
            else
            {
                model.CreateKraftExcel(order, 1);
                model.CreateKraftExcel(order, 2);
            }
            var context = new ApplicationDbContext();
            var allUsers = context.Users.Where(t => t.LockoutEndDateUtc == null).ToList();
            PushNotification.PushAndroidNotification(allUsers.Where(t => t.Platform == "android" && t.Token != null && t.Token != "").Select(t => t.Token).ToList(), ConfigurationManager.AppSettings["OrderMessage"]);
            PushNotification.PushAppleNotification(allUsers.Where(t => t.Platform == "ios" && t.Token != null && t.Token != "").Select(t => t.Token).ToList(), ConfigurationManager.AppSettings["OrderMessage"]);
        }


    }
}