﻿using POBusiness.Models;
using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSystem.Controllers
{
    [Authorize]
    [RoutePrefix("client")]
    public class ClientController : Controller
    {
        private IClientRepository _repository;
        public ClientController(IClientRepository repository)
        {
            _repository = repository;
        }

        
        // GET: Client
        public ActionResult Client()
        {
            return View();
        }

        [Route("get")]
        public JsonResult GetClients()
        {
            Client_Vendor_State client = new Client_Vendor_State();
            client.clients = _repository.All;
            client.states = _repository.GetStates();
            return Json(client, JsonRequestBehavior.AllowGet);
        }

        [Route("add")]
        [HttpPost]
        public JsonResult AddClient(Client client)
        {
            if (client.id > 0)
                _repository.Update(client);
            else
                _repository.Add(client);
            return Json(client, JsonRequestBehavior.AllowGet);
        }

        [Route("delete/{id:int}")]
        [HttpDelete]
        public JsonResult DeleteClient(int id)
        {
            _repository.Delete(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


    }
}