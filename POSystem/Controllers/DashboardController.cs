﻿using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSystem.Controllers
{
    [RoutePrefix("dashboard")]
    public class DashboardController : Controller
    {
        private IClientPORepository _repository;
        private IClientRepository _clientRepository;
        private IVendorRepository _vendorRepository;

        public DashboardController(IClientPORepository repository, IClientRepository clientRepository, IVendorRepository vendorRepository)
        {
            _repository = repository;
            _clientRepository = clientRepository;
            _vendorRepository = vendorRepository;
        }

        // GET: Dashboard
        public ActionResult Dashboard()
        {
            return View();
        }

        [Route("Data")]
        public JsonResult GetResult()
        {

            Dashboard_PO_Order model = new Dashboard_PO_Order();
            model.rejectedPOs = _repository.GetRejectedPOs().Where(t => t.client_po_detail.Count() > 0).ToList();
            model.rejectedOrders = _repository.GetRejectedOrders().Where(t => t.vendor_po_detail.Count() > 0).ToList();
            model.pendingDeliveries = _repository.GetPendingDeliveries();
            model.approvedOrders= _repository.GetApprovedOrders();
            model.approvedPOs= _repository.GetApprovedPO();
            model.rateTypes = _repository.AllRateTypes;
            model.unitTypes = _repository.AllUnitTypes;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}