﻿using POBusiness.Models;
using POBusiness.Models.ViewModels;
using POBusiness.Repository;
using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace POSystem.Controllers
{
    [Authorize]
    [RoutePrefix("Invoice")]
    public class InvoiceController : Controller
    {
        private IClientPORepository _repository;
        private IClientRepository _clientRepository;
        private IVendorRepository _vendorRepository;
        private IInvoiceRepository _invoiceRepository;
        public InvoiceController(IClientPORepository repository, IClientRepository clientRepository, IVendorRepository vendorRepository, IInvoiceRepository invoiceRepository)
        {
            _repository = repository;
            _clientRepository = clientRepository;
            _vendorRepository = vendorRepository;
            _invoiceRepository = invoiceRepository;
        }

        [ClaimsAuthorization(ClaimType = "Invoice", ClaimValue = "1")]
        [Route("Invoice")]
        public ActionResult Index()
        {
            return View("Invoice");
        }

        [HttpGet]
        [Route("getinvoice")]
        public JsonResult GetInvoice()
        {
            Invoice_Order invoiceOrder = new Invoice_Order();
            invoiceOrder.clients = _clientRepository.All;
            invoiceOrder.vendors = _vendorRepository.All;
            invoiceOrder.orders = _invoiceRepository.AllOrders;
            foreach (var item in invoiceOrder.orders)
            {
                var completeCount = item.vendor_po_detail.Where(t => t.status_id == 3).Count();
                if (completeCount == item.vendor_po_detail.Count())
                    item.statusname = "Completed";
                else if (item.vendor_po_detail.Where(t => t.status_id == 4).Count() == item.vendor_po_detail.Count())
                    item.statusname = "Ordered";
                else
                    item.statusname = "Partial";
            }
            return Json(invoiceOrder, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("markcomplete/{orderId:int}")]
        public JsonResult MarkComplete(int orderId)
        {
            _repository.MarkAsComplete(orderId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("updateinvoice")]
        public JsonResult UpdateInvoiceDetails(OrderDetail order)
        {
            _repository.UpdateOrder(order);
            return Json(order, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        [Route("remove/{deleteStr}")]
        public JsonResult RemoveCompleted(string deleteStr)
        {
            _invoiceRepository.RemoveCompleted(deleteStr);            
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}