﻿using POBusiness.Models;
using POBusiness.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSystem.Controllers
{
    [RoutePrefix("report")]
    public class ReportController : Controller
    {
        private IClientRepository _clientRepository;
        private IVendorRepository _vendorRepository;
        private IReportRepository _reportRepository;
        public ReportController(IClientRepository clientRepository, IVendorRepository vendorRepository, IReportRepository reportRepository)
        {
            _clientRepository = clientRepository;
            _vendorRepository = vendorRepository;
            _reportRepository = reportRepository;
        }
        public ActionResult Report()
        {
            return View();
        }

        [Route("getData")]
        public JsonResult GetData()
        {
            Report report = new POBusiness.Models.Report();
            report.clients = _clientRepository.All;
            report.vendors = _vendorRepository.All;
            
            return Json(report, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("search")]
        public JsonResult GetRecords(Report report)
        {
            try
            {

            var res = _reportRepository.SearchReports(report);
            return Json(res, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex = ex;
            }
            return null;
        }
    }
}