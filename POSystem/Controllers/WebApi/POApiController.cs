﻿using Microsoft.AspNet.Identity;
using POBusiness.Models;
using POBusiness.Models.ApiModel;
using POBusiness.Repository;
using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSystem.Controllers
{
    //[Authorize]
    [RoutePrefix("api")]
    public class POApiController : ApiController
    {
        private IClientPORepository _repository;
        //public POApiController() : base()
        //{
        //}
        public POApiController(IClientPORepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("podetailscount")]
        public HttpResponseMessage GetPODetailsCount()
        {
            //if (User.Identity.IsAuthenticated)
            //{
                dynamic res = _repository.GetDetailsCount();

                return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
                {
                    Message = "Success",
                    Status = Convert.ToBoolean(ActionStatus.Successfull),
                    Result = res

                });
            //}
            //else
            //{
            //    return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            //    {
            //        Message = "Failed",
            //        Status = Convert.ToBoolean(ActionStatus.Unauthorized),
            //        Result = null,
            //        Code = 401
            //    });
            //}
        }
        [HttpGet]
        [Route("podetails/{status:int}/{total_records:int}/{page_number:int}")]
        public HttpResponseMessage GetPODetails(int status, int total_records, int page_number)
        {
            //if (User.Identity.IsAuthenticated)
            //{
                dynamic res = _repository.GetPODetails(status, total_records, page_number);

                return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
                {
                    Message = "Success",
                    Status = Convert.ToBoolean(ActionStatus.Successfull),
                    Result = res
                });
            //}
            //else
            //{
            //    return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            //    {
            //        Message = "Failed",
            //        Status = Convert.ToBoolean(ActionStatus.Unauthorized),
            //        Result = null,
            //        Code = 401
            //    });
            //}
        }

        [HttpPost]
        [Route("updatestatus")]
        public HttpResponseMessage UpdateStatus(StatusAppReject status)
        {
            //if (User.Identity.IsAuthenticated)
            //{
                _repository.UpdateStatus(status);
                return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
                {
                    Message = "Success",
                    Status = Convert.ToBoolean(ActionStatus.Successfull),
                    Result = null
                });
            //}
            //else
            //{
            //    return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            //    {
            //        Message = "Failed",
            //        Status = Convert.ToBoolean(ActionStatus.Unauthorized),
            //        Result = null,
            //        Code = 401
            //    });
            //}
        }

    }
}
