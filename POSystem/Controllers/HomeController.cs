﻿using POBusiness.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSystem.Controllers
{
    public class HomeController : Controller
    {
        private IPurchaseOrderRepository _purchaseOrder;

        public HomeController(IPurchaseOrderRepository purchaseOrder)
        {
            _purchaseOrder = purchaseOrder;
        }
        public ActionResult Index()
        {
            //var res = _purchaseOrder.All;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}