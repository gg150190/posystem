﻿using POSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace POSystem.Controllers
{
    [RoutePrefix("user")]
    public class UserController : Controller
    {
        // GET: User
        [ClaimsAuthorization(ClaimType = "UM", ClaimValue = "1")]
        public ActionResult Index()
        {
            
            return View("UserManagement");
        }

        [Route("users")]
        [HttpGet]
        public JsonResult GetAllUsers()
        {
            var context = new ApplicationDbContext();
            var userId = User.Identity.GetUserId<int>();
            var allUsers = context.Users.Where(t => t.LockoutEndDateUtc == null && t.Id != userId).ToList();
            return Json(allUsers, JsonRequestBehavior.AllowGet);
        }


    }
}