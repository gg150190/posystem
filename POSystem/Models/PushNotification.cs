﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace POSystem.Models
{
    public class PushNotification
    {
        public static void PushAppleNotification(List<string> deviceIds, string message)
        {
            try
            {

                var succeeded = 0;
                var failed = 0;
                var attempted = 0;
                var filePath = System.Web.HttpContext.Current.Server.MapPath("/Content/POProductionPush.p12");
                var password = ConfigurationManager.AppSettings["AppleCertificatePassword"];
                var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, filePath, password);
                var broker = new ApnsServiceBroker(config);
                broker.OnNotificationFailed += (notification, exception) =>
                {
                    failed++;
                };
                broker.OnNotificationSucceeded += (notification) =>
                {
                    succeeded++;
                };
                broker.Start();

                foreach (var dt in deviceIds)
                {
                    attempted++;
                    broker.QueueNotification(new ApnsNotification
                    {
                        DeviceToken = dt,
                        Payload = JObject.Parse("{ \"aps\" : { \"alert\" : \"'" + message + "'\" } }")
                    });
                }

                broker.Stop();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void PushAndroidNotification(List<string> deviceIds, string message)
        {
            var succeeded = 0;
            var failed = 0;
            var attempted = 0;

            var config = new GcmConfiguration("", ConfigurationManager.AppSettings["AndroidServerKey"], null);
            var broker = new GcmServiceBroker(config);
            broker.OnNotificationFailed += (notification, exception) =>
            {
                failed++;
            };
            broker.OnNotificationSucceeded += (notification) =>
            {
                succeeded++;
            };

            broker.Start();

            foreach (var regId in deviceIds)
            {
                attempted++;

                broker.QueueNotification(new GcmNotification
                {
                    RegistrationIds = new List<string> {
                        regId
                    },
                    Data = JObject.Parse("{ \"aps\" : { \"Notification\" : \"'" + message + "'\" } }")
                });
            }

            broker.Stop();
        }
    }
}