﻿using POBusiness.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using ExcelRef = OfficeOpenXml;
namespace POSystem.Models
{
    public class ExcelModel
    {
        public void CreateExcel(Order order, int type)
        {
            try
            {
                ExcelRef.ExcelPackage excel = new ExcelRef.ExcelPackage();
                ExcelRef.ExcelWorksheet sh = excel.Workbook.Worksheets.Add("Order");
                var picture = sh.Drawings.AddPicture("", Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/Images/excel_logo.png")));
                picture.SetPosition(1, 0, 0, 5);
                // sh.SelectedRange[1, 1, 2, 1].Merge = true;
                sh.SelectedRange["B1:I1"].Value = "SHARVI PACKAGING SOLUTIONS PVT LTD";
                sh.SelectedRange["B1:I1"].Merge = true;
                sh.SelectedRange["B2:I2"].Value = "ORDER BOOKING FORM";
                sh.SelectedRange["B2:I2"].Merge = true;
                sh.SelectedRange["J1:K1"].Value = "ORDER DATE";
                sh.SelectedRange["J1:K1"].Merge = true;
                sh.SelectedRange["J2:K2"].Value = "PURCHASE ORDER No.";
                sh.SelectedRange["J2:K2"].Merge = true;
                sh.SelectedRange["L1:M1"].Merge = true;
                sh.Cells["L1"].Style.Numberformat.Format = "dd/mm/yyyy";
                sh.Cells["L1"].Value = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
                sh.SelectedRange["L2:M2"].Merge = true;
                sh.SelectedRange["L2:M2"].Value = type == 1 ? order.client_po_number : order.po_number;
                //sh.SelectedRange["A3:C3"].Value = "BUYER/BILL TO PARTY";
                //sh.SelectedRange["A3:C3"].Merge = true;
                sh.SelectedRange["D3:F3"].Value = "BUYER/BILL TO PARTY";
                sh.SelectedRange["D3:F3"].Merge = true;
                sh.SelectedRange[4, 4, 7, 6].Merge = true;
                sh.SelectedRange[4, 4, 7, 6].Value = order.Buyer.id > 0 ? (order.Buyer.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.address_1) == null ? "" : order.Buyer.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.address_2) == null ? "" : order.Buyer.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.city) == null ? "" : order.Buyer.city)) : "";
                sh.SelectedRange[4, 4, 7, 6].Style.WrapText = true;

                sh.SelectedRange["G3:J3"].Value = "CONSIGNEE NAME & ADDRESS";
                sh.SelectedRange["G3:J3"].Merge = true;
                sh.SelectedRange[4, 7, 7, 10].Merge = true;
                sh.SelectedRange[4, 7, 7, 10].Value = order.Consignee.id > 0 ? (order.Consignee.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.address_1) == null ? "" : order.Consignee.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.address_2) == null ? "" : order.Consignee.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.city) == null ? "" : order.Consignee.city)) : "";
                sh.SelectedRange[4, 7, 7, 10].Style.WrapText = true;

                sh.SelectedRange["K3:M3"].Merge = true;
                sh.SelectedRange["K3:M3"].Value = "DELIVERY ADDRESS";
                sh.SelectedRange[4, 11, 7, 13].Merge = true;
                //sh.SelectedRange[4, 11, 7, 13].Value = order.delivery_address;
                sh.SelectedRange[4, 11, 7, 13].Value = order.Delivery.id > 0 ? (order.Delivery.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.address_1) == null ? "" : order.Delivery.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.address_2) == null ? "" : order.Delivery.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.city) == null ? "" : order.Delivery.city)) : "";
                sh.SelectedRange[4, 11, 7, 13].Style.WrapText = true;
                //sh.SelectedRange[4, 1, 7, 3].Merge = true;
                //sh.SelectedRange[4, 1, 7, 3].Value = order.Buyer.id > 0 ? (order.Buyer.name + System.Environment.NewLine +
                //                                                 (Convert.ToString(order.Buyer.address_1) == null ? "" : order.Buyer.address_1) + System.Environment.NewLine +
                //                                                 (Convert.ToString(order.Buyer.address_2) == null ? "" : order.Buyer.address_2)) : "";

                sh.Cells[9, 4].Value = "ECC No.";
                sh.Cells[11, 4].Value = "GST No.";
                sh.Cells[13, 4].Value = "PAN No.";
                sh.Cells[13, 5].Value = Convert.ToString(order.Buyer.pan) == null ? "" : order.Buyer.pan;
                sh.SelectedRange["E9:F9"].Merge = true;
                sh.SelectedRange["E9:F9"].Value = order.Buyer.id > 0 ? (Convert.ToString(order.Buyer.ex_number) == null ? "" : order.Buyer.ex_number) : "";
                sh.SelectedRange["E10:F10"].Merge = true;
                sh.SelectedRange["E11:F11"].Merge = true;
                sh.SelectedRange["E11:F11"].Value = order.Buyer.id > 0 ? (Convert.ToString(order.Buyer.tin_number) == null ? "" : order.Buyer.tin_number) : "";
                sh.SelectedRange["E12:F12"].Merge = true;
                sh.SelectedRange["E13:F13"].Merge = true;

                sh.Cells[9, 7].Value = "ECC No.";
                sh.Cells[11, 7].Value = "GST No.";
                sh.Cells[13, 7].Value = "PAN No.";
                sh.Cells[13, 8].Value = Convert.ToString(order.Consignee.pan) == null ? "" : order.Consignee.pan;
                sh.SelectedRange["H9:J9"].Merge = true;
                sh.SelectedRange["H9:J9"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Consignee.ex_number) == null ? "" : order.Consignee.ex_number) : "";
                sh.SelectedRange["H10:J10"].Merge = true;
                sh.SelectedRange["H11:J11"].Merge = true;
                sh.SelectedRange["H11:J11"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Consignee.tin_number) == null ? "" : order.Consignee.tin_number) : "";
                sh.SelectedRange["H12:J12"].Merge = true;
                sh.SelectedRange["H13:J13"].Merge = true;

                sh.Cells[8, 11].Value = "Delivery Place";
                sh.Cells[8, 12].Value = "";
                sh.Cells[8, 13].Value = "";

                sh.Cells[9, 11].Value = "ECC No.";
                sh.Cells[11, 11].Value = "GST No.";
                sh.Cells[13, 11].Value = "PAN No.";
                sh.Cells[13, 12].Value = Convert.ToString(order.Delivery.pan) == null ? "" : order.Delivery.pan;
                sh.SelectedRange["L9:M9"].Merge = true;
                //sh.SelectedRange["L9:M9"].Value = Convert.ToString(order.vendor.ex_number) == null ? "" : order.vendor.ex_number;
                sh.SelectedRange["L9:M9"].Value = order.Delivery.id > 0 ? (Convert.ToString(order.Delivery.ex_number) == null ? "" : order.Delivery.ex_number) : "";
                sh.SelectedRange["L10:M10"].Merge = true;
                sh.SelectedRange["L11:M11"].Merge = true;
                //sh.SelectedRange["L11:M11"].Value = Convert.ToString(order.vendor.tin_number) == null ? "" : order.vendor.tin_number;
                sh.SelectedRange["L11:M11"].Value = order.Delivery.id > 0 ? (Convert.ToString(order.Delivery.tin_number) == null ? "" : order.Delivery.tin_number) : "";
                sh.SelectedRange["L12:M12"].Merge = true;
                sh.SelectedRange["L13:M13"].Merge = true;

                sh.SelectedRange["A14:M14"].Merge = true;
                sh.Cells[15, 1].Value = "S. No.";
                sh.SelectedRange["B15:C15"].Merge = true;
                sh.SelectedRange["B15:C15"].Value = "GSM";
                sh.Cells[15, 4].Value = "Quality";
                sh.Cells[15, 5].Value = "Units";
                sh.Cells[15, 6].Value = "Quantity(in Tons)";
                sh.SelectedRange["G15:I15"].Merge = true;
                sh.SelectedRange["G15:I15"].Value = "Size (CMS)";
                sh.Cells[15, 10].Value = "Ream Weight";
                //sh.Cells[15, 11].Value = "";
                sh.SelectedRange["K15:M15"].Merge = true;
                sh.SelectedRange["K15:M15"].Value = "Remarks";
                var count = order.vendor_po_detail.Count();
                var initialize = 16;
                for (int i = 0; i < count; i++)
                {
                    for (int j = 1; j <= 11; j++)
                    {
                        if (j != 2)
                        {
                            if (j == 3)
                            {
                                sh.SelectedRange["B" + initialize + ":C" + initialize].Merge = true;
                                sh.SelectedRange["B" + initialize + ":C" + initialize].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].gsm) == null ? "" : order.vendor_po_detail.ToList()[i].gsm;
                            }
                            if (j == 8)
                                sh.Cells[initialize, j].Value = "X";
                            if (j == 1)
                                sh.Cells[initialize, j].Value = i + 1;
                            if (j == 4)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].quality) == null ? "" : order.vendor_po_detail.ToList()[i].quality;
                            if (j == 5)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].unitname) == null ? "" : order.vendor_po_detail.ToList()[i].unitname;
                            if (j == 6)
                                sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].unit == 4 ?
                                                                (order.vendor_po_detail.ToList()[i].ordered_quantity * Convert.ToDecimal(order.vendor_po_detail.ToList()[i].rim_weight)) :
                                                                (order.vendor_po_detail.ToList()[i].unit == 2 ? (Convert.ToDecimal(0.001) * order.vendor_po_detail.ToList()[i].ordered_quantity) : order.vendor_po_detail.ToList()[i].ordered_quantity);
                            if (j == 7)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].paper_length) == null ? 0 : (order.vendor_po_detail.ToList()[i].size == "1" ? (Convert.ToDouble(order.vendor_po_detail.ToList()[i].paper_length) * 2.54) : Convert.ToDouble(order.vendor_po_detail.ToList()[i].paper_length));
                            if (j == 9)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].paper_breadth) == null ? 0 : (order.vendor_po_detail.ToList()[i].size == "1" ? (Convert.ToDouble(order.vendor_po_detail.ToList()[i].paper_breadth) * 2.54) : Convert.ToDouble(order.vendor_po_detail.ToList()[i].paper_breadth));
                            if (j == 10)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].rim_weight) == null ? "" : order.vendor_po_detail.ToList()[i].rim_weight;
                            //if (j == 11)
                            //    sh.Cells[initialize, j].Value = "";
                        }
                    }
                    initialize++;
                }
                initialize = initialize + 3;
                sh.SelectedRange[16, 11, (initialize - 1), 13].Merge = true;
                sh.SelectedRange[16, 11, (initialize - 1), 13].Value = order.notes;
                sh.SelectedRange[16, 11, (initialize - 1), 13].Style.WrapText = true;
                sh.SelectedRange[16, 11, (initialize - 1), 13].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;

                sh.SelectedRange[initialize, 1, initialize + 8, 3].Merge = true;
                sh.SelectedRange[initialize, 1, initialize + 8, 3].Value = "Special Instructions(Order / Delivery)";
                sh.SelectedRange[initialize, 1, initialize + 8, 3].Style.WrapText = true;

                sh.SelectedRange[initialize, 4, initialize + 8, 13].Merge = true;


                sh.SelectedRange[initialize, 4, initialize + 8, 13].Value =
                "1) Quality Report to be sent along with lorry." + System.Environment.NewLine +
                "2) Instruct transporter to make entry in the name of Sharvi Packaging Solutions Pvt.Ltd." + System.Environment.NewLine +
                "3) Truck wala must be contact to: Mr.Praveen Jha 9805087786 Mr.Ankur 9418355586 before reaching Himachal or Tri City Chandigarh" + System.Environment.NewLine +
                "4) Assure that All Forms at Border should be passed in the Name of Sharvi Packaging Solutions Pvt.Ltd." + System.Environment.NewLine +
                "5) Assure that All Bilty should be Endrose Properly (if Sale in Transit)" + System.Environment.NewLine +
                "6) Please dispatch full quantity at once, part quantity not acceptable" + System.Environment.NewLine +
                "7) GSM to be maintain on plus side"
                ;
                sh.SelectedRange[initialize, 4, initialize + 8, 13].Style.WrapText = true;
                sh.SelectedRange["A" + (initialize + 9) + ":F" + (initialize + 9)].Merge = true;
                sh.SelectedRange["A" + (initialize + 9) + ":F" + (initialize + 9)].Value = "Invoice Type : E-1";
                sh.SelectedRange["G" + (initialize + 9) + ":M" + (initialize + 9)].Merge = true;
                sh.SelectedRange["A" + (initialize + 10) + ":C" + (initialize + 10)].Merge = true;
                sh.SelectedRange["A" + (initialize + 10) + ":C" + (initialize + 10)].Value = "Delivery Date";
                sh.Cells[(initialize + 10), 4].Value = "Urgent";
                MergeWithValue("E" + (initialize + 10) + ":G" + (initialize + 10), "Pass Maximum Discount in Bill", sh);
                sh.Cells[(initialize + 10), 8].Value = "";
                sh.SelectedRange["I" + (initialize + 10) + ":J" + (initialize + 10)].Merge = true;
                sh.SelectedRange["K" + (initialize + 10) + ":L" + (initialize + 10)].Merge = true;
                sh.Cells[(initialize + 10), 13].Value = "";
                sh.SelectedRange["A" + (initialize + 11) + ":M" + (initialize + 11)].Merge = true;
                MergeWithValue("A" + (initialize + 12) + ":M" + (initialize + 12), "FOR BRANCH OFFICE USE", sh);
                MergeWithValue("A" + (initialize + 13) + ":D" + (initialize + 13), "SALES ORDER NUMBER", sh);
                sh.SelectedRange["E" + (initialize + 13) + ":I" + (initialize + 13)].Merge = true;
                MergeWithValue("J" + (initialize + 13) + ":K" + (initialize + 13), "S. O. DATE", sh);
                sh.SelectedRange["L" + (initialize + 13) + ":M" + (initialize + 13)].Merge = true;

                var range = sh.SelectedRange[1, 1, (initialize + 13), 13];
                range.Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.Cells[1, 1, (initialize + 13), 13].AutoFitColumns();
                sh.Column(3).Width = 10;
                //int rowsCount = type == 1 ? initialize : initialize + 13;
                int rowsCount = initialize + 13;

                sh.Cells[1, 1, rowsCount, 13].Style.Border.Top.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 13].Style.Border.Left.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 13].Style.Border.Right.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 13].Style.Border.Bottom.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                //sh.Cells[1, 1, (initialize + 13), 13].Table.ColumnSpan = "18";//.AutoFitColumns(50);
                sh.SelectedRange[initialize, 4, initialize + 8, 13].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;

                sh.SelectedRange[4, 1, 13, 3].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;
                sh.SelectedRange[4, 11, 7, 13].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;
                sh.SelectedRange[initialize, 1, initialize + 8, 3].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;
                sh.SelectedRange[initialize, initialize, (initialize + 8), (initialize + 8)].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;
                sh.Row(10).Hidden = true;
                sh.Row(12).Hidden = true;
                var path = System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/PO_" + (type == 1 ? "Client_" : "Vendor_") + (type == 1 ? order.client_po_number.Replace("/", "-") : order.po_number.Replace("/", "-")) + ".xlsx");
                Stream stream = File.Create(path);
                excel.SaveAs(stream);
                stream.Close();
                //sendEmail(order.vendorEmail, path);

            }
            catch (Exception ex)
            {
                string createText = ex.Message;
                File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/Logger.txt"), createText + Environment.NewLine);
            }
        }
        public void CreateKraftExcel(Order order, int type)
        {
            try
            {
                ExcelRef.ExcelPackage excel = new ExcelRef.ExcelPackage();
                ExcelRef.ExcelWorksheet sh = excel.Workbook.Worksheets.Add("Order");
                sh.SelectedRange["K5:K8"].Merge = true;
                var picture = sh.Drawings.AddPicture("", Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/Images/excel_logo_short.png")));
                picture.SetPosition(4, 1, 10, 23);
                sh.SelectedRange["A1:K1"].Merge = true;
                if (type == 2)
                {
                    sh.SelectedRange["A1:K1"].Value = "PURCHASE ORDER";
                }
                else
                {
                    sh.SelectedRange["A1:K1"].Value = "ORDER CONFIRMATION";
                }
                sh.SelectedRange["A2:K2"].Merge = true;
                sh.SelectedRange["A2:K2"].Value = "SHARVI TRADERS PRIVATE LIMITED";
                sh.SelectedRange["A3:F3"].Merge = true;
                sh.SelectedRange["A4:F4"].Merge = true;
                sh.SelectedRange["A4:F4"].Value = "Vendor Name & Address";
                sh.Cells[5, 1, 8, 1].Merge = true;
                sh.Cells[5, 1].Value = "M/S";
                sh.Cells[5, 1].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Top;
                sh.Cells[9, 1].Value = "PAN No. :";
                sh.SelectedRange["B9:F9"].Merge = true;
                sh.Cells[9, 2].Value = Convert.ToString(order.vendor.pan) == null ? "" : order.vendor.pan;
                sh.SelectedRange[5, 2, 8, 6].Merge = true;
                sh.SelectedRange[5, 2, 8, 6].Value = order.vendor.name + System.Environment.NewLine +
                                                     (Convert.ToString(order.vendor.address_1) == null ? "" : Convert.ToString(order.vendor.address_1)) + System.Environment.NewLine +
                                                    (Convert.ToString(order.vendor.address_2) == null ? "" : Convert.ToString(order.vendor.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.vendor.city) == null ? "" : order.vendor.city));
                sh.SelectedRange[5, 2, 7, 6].Style.WrapText = true;
                sh.Cells[10, 1].Value = "GST No. :";
                sh.SelectedRange["B10:F10"].Merge = true;
                sh.SelectedRange["B10:F10"].Value = Convert.ToString(order.vendor.tin_number) == null ? "" : Convert.ToString(order.vendor.tin_number);
                sh.Cells[11, 1].Value = "ECC No. :";
                sh.SelectedRange["B11:F11"].Merge = true;
                sh.SelectedRange["B11:F11"].Value = Convert.ToString(order.vendor.ex_number) == null ? "" : Convert.ToString(order.vendor.ex_number);
                sh.Cells[3, 7].Value = "Order No. :";
                sh.Cells[3, 10].Value = "Date: ";
                sh.Cells[3, 10].Style.Font.Bold = true;
                sh.SelectedRange["H3:H3"].Merge = true;
                sh.SelectedRange["H3:H3"].Value = type == 1 ? (Convert.ToString(order.client_po_number) == null ? "" : Convert.ToString(order.client_po_number)) : (Convert.ToString(order.po_number) == null ? "" : Convert.ToString(order.po_number));
                sh.SelectedRange["I3:I3"].Merge = true;
                sh.SelectedRange["I3:I3"].Value = type == 1 ? "" : (Convert.ToString(order.manual_po_number) == null ? "" : Convert.ToString(order.manual_po_number));
                //sh.Cells[9, 11].Value = DateTime.Now;

                sh.Cells[3, 11].Style.Numberformat.Format = "dd/mm/yyyy";
                sh.Cells[3, 11].Value = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;

                sh.Cells[4, 7, 4, 10].Merge = true;

                sh.Cells[4, 7, 4, 10].Value = "Buyer / Bill To Name & Address";
                sh.Cells[5, 7, 8, 7].Merge = true;
                sh.Cells[5, 7].Value = "M/S";
                sh.Cells[5, 7].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Top;
                sh.SelectedRange[5, 8, 8, 10].Merge = true;
                //                sh.SelectedRange[5, 8, 7, 10].Style.Border.Right.Style = ExcelRef.Style.ExcelBorderStyle.Dashed;
                //sh.SelectedRange[12, 2, 14, 6].Value = "Sharvi Traders Private Limited " + System.Environment.NewLine + "Plo No. 193, Krishna Colony, Lane No. 2 " + System.Environment.NewLine + "Opp. Sector -25, Faridabad (HR)";
                sh.SelectedRange[5, 8, 8, 10].Value = order.Buyer.id > 0 ? (order.Buyer.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.address_1) == null ? "" : order.Buyer.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.address_2) == null ? "" : order.Buyer.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Buyer.city) == null ? "" : order.Buyer.city)) : "";
                sh.SelectedRange[5, 8, 7, 10].Style.WrapText = true;
                sh.Cells[9, 7].Value = "PAN No. :";
                sh.SelectedRange["H9:K9"].Merge = true;
                //sh.SelectedRange["B15:F15"].Value = "";
                sh.Cells[9, 8].Value = Convert.ToString(order.Buyer.pan) == null ? "" : order.Buyer.pan;

                sh.Cells[10, 7].Value = "GST No. :";
                sh.SelectedRange["H10:K10"].Merge = true;
                //sh.SelectedRange["B16:F16"].Value = "98989898989";
                sh.SelectedRange["H10:K10"].Value = order.Buyer.id > 0 ? (Convert.ToString(order.Buyer.tin_number) == null ? "" : order.Buyer.tin_number) : "";
                sh.SelectedRange["H11:K11"].Merge = true;
                //sh.SelectedRange["B17:F17"].Value = "AAJCS5488AXD001";
                sh.SelectedRange["H11:K11"].Value = order.Buyer.id > 0 ? (Convert.ToString(order.Buyer.ex_number) == null ? "" : order.Buyer.ex_number) : "";
                sh.Cells[11, 1, 11, 6].Merge = true;
                sh.Cells[11, 1, 11, 5].Value = "Consignee / Ship To Name & Address";
                sh.Cells[12, 1, 15, 1].Merge = true;
                sh.Cells[12, 1].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Top;
                
                sh.Cells[12, 1].Value = "M/S";
                sh.SelectedRange[12, 2, 15, 6].Merge = true;
                //sh.SelectedRange[12, 8, 14, 11].Value = "";
                sh.SelectedRange[12, 2, 15, 6].Value = order.Consignee.id > 0 ? (order.Consignee.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.address_1) == null ? "" : order.Consignee.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.address_2) == null ? "" : order.Consignee.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Consignee.city) == null ? "" : order.Consignee.city)) : "";
                sh.SelectedRange[12, 2, 14, 5].Style.WrapText = true;
                sh.Cells[16, 1].Value = "PAN No. :";
                sh.SelectedRange["B16:F16"].Merge = true;
                //sh.SelectedRange["H15:K15"].Value = "";
                sh.Cells[16, 2].Value = Convert.ToString(order.Consignee.pan) == null ? "" : order.Consignee.pan;
                //sh.Cells[18, 1].Value = "TIN No. :";
                //sh.SelectedRange["B18:F18"].Merge = true;
                ////sh.SelectedRange["H16:K16"].Value = "";
                //sh.SelectedRange["B18:F18"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Consignee.tin_number) == null ? "" : order.Consignee.tin_number) : "";
                //sh.Cells[19, 1].Value = "ECC No. :";
                //sh.SelectedRange["B19:F19"].Merge = true;
                ////sh.SelectedRange["H17:K17"].Value = "";
                //sh.SelectedRange["B19:E19"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Consignee.ex_number) == null ? "" : order.Consignee.ex_number) : "";
                sh.Cells[11, 7, 11, 11].Merge = true;

                sh.Cells[11, 7, 11, 11].Value = "Delivery Address";
                sh.Cells[12, 7, 15, 7].Merge = true;
                sh.Cells[12, 7].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Top;
                sh.Cells[12, 7].Value = "M/S";
                sh.SelectedRange[12, 8, 15, 11].Merge = true;
                //sh.SelectedRange[20, 2, 22, 6].Value = Convert.ToString(order.delivery_address) == null ? "" : Convert.ToString(order.delivery_address);
                sh.SelectedRange[12, 8, 15, 11].Value = order.Delivery.id > 0 ? (order.Delivery.name + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.address_1) == null ? "" : order.Delivery.address_1) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.address_2) == null ? "" : order.Delivery.address_2) + System.Environment.NewLine +
                                                                 (Convert.ToString(order.Delivery.city) == null ? "" : order.Delivery.city)) : "";
                sh.SelectedRange[12, 8, 15, 11].Style.WrapText = true;
                sh.Cells[16, 7].Value = "Cont. No. :";
                sh.SelectedRange["H16:K16"].Merge = true;
                //sh.Cells[18, 7].Value = "TIN No. :";
                //sh.SelectedRange["H18:K18"].Merge = true;
                ////sh.SelectedRange["H16:K16"].Value = "";
                //sh.SelectedRange["H18:K18"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Delivery.tin_number) == null ? "" : order.Delivery.tin_number) : "";
                //sh.Cells[19, 7].Value = "ECC No. :";
                //sh.SelectedRange["H19:K19"].Merge = true;
                ////sh.SelectedRange["H17:K17"].Value = "";
                //sh.SelectedRange["H19:K19"].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Delivery.ex_number) == null ? "" : order.Delivery.ex_number) : "";
                //sh.SelectedRange["B23:F23"].Value = "";
                sh.Row(17).Hidden = true;
                sh.Cells[18, 1].Value = "GST No. :";
                sh.Cells[18, 2, 18, 11].Merge = true;
                sh.Cells[18,2].Value = order.Consignee.id > 0 ? (Convert.ToString(order.Consignee.tin_number) == null ? "" : order.Consignee.tin_number) : "";
                sh.SelectedRange["H17:K17"].Value = order.Delivery.id > 0 ? (Convert.ToString(order.Delivery.contact_number) == null ? "" : order.Delivery.contact_number) : "";
                sh.Cells[19, 1, 19, 2].Merge = true;
                sh.Cells[19, 3, 19, 3].Merge = true;
                sh.Cells[19, 4, 19, 5].Merge = true;
                sh.Cells[19, 6, 19, 6].Merge = true;
                sh.Cells[19, 7, 19, 7].Merge = true;
                sh.Cells[19, 8, 19, 8].Merge = true;
                sh.Cells[19, 9, 19, 9].Merge = true;
                sh.Cells[19, 10, 19, 10].Merge = true;
                sh.Cells[19, 11, 19, 11].Merge = true;

                sh.Cells[19, 1, 19, 2].Value = "Quality";
                sh.Cells[19, 3, 19, 3].Value = "GSM";
                sh.Cells[19, 4, 19, 5].Value = "Size";
                sh.Cells[19, 6, 19, 6].Value = "Size Type";
                sh.Cells[19, 7, 19, 7].Value = "Quantity";
                sh.Cells[19, 8, 19, 8].Value = "Units";
                sh.Cells[19, 9, 19, 9].Value = "Rate (PKgs)";
                sh.Cells[19, 10, 19, 10].Value = "Rate Type";
                sh.Cells[19, 11, 19, 11].Value = "Delivery Date";

                var count = order.vendor_po_detail.Count();
                decimal totatQuantity = 0;
                var initialize = 20;
                for (int i = 0; i < count; i++)
                {
                    for (int j = 1; j <= 11; j++)
                    {
                        if (j != 1 && j != 4)
                        {
                            if (j == 3)
                            {
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].gsm) == null ? "" : order.vendor_po_detail.ToList()[i].gsm;
                            }
                            if (j == 8)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].unitname) == null ? "" : order.vendor_po_detail.ToList()[i].unitname;
                            if (j == 2)
                            {
                                sh.Cells[initialize, j - 1, initialize, j].Merge = true;
                                sh.Cells[initialize, j - 1, initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].quality) == null ? "" : order.vendor_po_detail.ToList()[i].quality;
                            }
                            if (j == 6)
                                sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].size == "1" ? "inch" : "cm";
                            if (j == 5)
                            {
                                sh.Cells[initialize, j - 1, initialize, j].Merge = true;
                                sh.Cells[initialize, j - 1, initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].paper_length) == null ? "" : order.vendor_po_detail.ToList()[i].paper_length;
                            }
                            if (j == 7)
                            {
                                sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].ordered_quantity;
                                totatQuantity += order.vendor_po_detail.ToList()[i].ordered_quantity;
                            }
                            if (j == 9)
                                sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].rate.ToString("F");
                            if (j == 10)
                                sh.Cells[initialize, j].Value = Convert.ToString(order.vendor_po_detail.ToList()[i].rateType2) == null ? "" : order.vendor_po_detail.ToList()[i].rateType2;
                            if (j == 11)
                            {
                                //sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].delivery_date;
                                sh.Cells[initialize, j].Style.Numberformat.Format = "dd/mm/yyyy";
                                sh.Cells[initialize, j].Value = order.vendor_po_detail.ToList()[i].delivery_date.Day + "/" + order.vendor_po_detail.ToList()[i].delivery_date.Month + "/" + order.vendor_po_detail.ToList()[i].delivery_date.Year;

                            }
                        }
                    }
                    initialize++;
                }

                sh.Cells[initialize, 1, initialize, 6].Merge = true;
                sh.Cells[initialize, 1, initialize, 6].Value = "Total";
                sh.Cells[initialize, 7].Value = totatQuantity;
                sh.Cells[initialize, 8, initialize, 11].Merge = true;

                if (type == 2)
                {
                    sh.Cells[initialize + 1, 1, initialize + 7, 2].Merge = true;
                    sh.Cells[initialize + 1, 1, initialize + 7, 2].Value = "Remarks if any:	";

                    sh.Cells[initialize + 1, 3, initialize + 7, 11].Merge = true;
                    sh.Cells[initialize + 1, 3, initialize + 7, 11].Value = order.notes;
                    sh.Cells[initialize + 1, 3, initialize + 7, 11].Style.WrapText = true;

                    sh.Cells[initialize + 8, 1, initialize + 8, 2].Merge = true;
                    sh.Cells[initialize + 8, 1, initialize + 8, 2].Value = "Freight Type:";

                    sh.Cells[initialize + 8, 3, initialize + 8, 11].Merge = true;
                    sh.Cells[initialize + 8, 3, initialize + 8, 11].Value = order.freightName;
                }
                else
                {
                    sh.Cells[initialize + 1, 1, initialize + 1, 2].Merge = true;
                    sh.Cells[initialize + 1, 1, initialize + 1, 2].Value = "Freight Type:";

                    sh.Cells[initialize + 1, 3, initialize + 1, 11].Merge = true;
                    sh.Cells[initialize + 1, 3, initialize + 1, 11].Value = order.freightName;
                }
                int rowsCount = type == 1 ? initialize : initialize + 8;
                sh.Cells[1, 1, rowsCount, 11].AutoFitColumns();
                sh.Cells[1, 1, rowsCount, 11].Style.Border.Top.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 11].Style.Border.Left.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 11].Style.Border.Right.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 11].Style.Border.Bottom.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[1, 1, rowsCount, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.Cells[initialize + 1, 3, initialize + 7, 11].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Center;
                sh.SelectedRange["A1:J1"].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.SelectedRange["A2:J2"].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.SelectedRange["A4:F4"].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                #region Alignments
                sh.Cells[5, 1].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[9, 1].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[10, 1].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[11, 1].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[11, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[5, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[9, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[10, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[12, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[12, 1].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[4, 7].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[16, 1, 18, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[13, 2, 16, 6].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[5, 2, 8, 6].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[5, 8, 8, 10].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[13, 8, 16, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[5, 2, 19, 6].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[5, 8, 19, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[3, 8, 3, 9].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[3, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.Cells[initialize + 8, 3, initialize + 8, 11].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                sh.SelectedRange[13, 2, 16, 6].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Bottom;
                sh.SelectedRange[5, 2, 8, 6].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Bottom;
                sh.SelectedRange[5, 8, 8, 10].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Bottom;
                sh.SelectedRange[13, 8, 16, 11].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Bottom;
                sh.Cells[initialize + 1, 1, initialize + 7, 2].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.Cells[initialize + 1, 1, initialize + 7, 2].Style.VerticalAlignment = ExcelRef.Style.ExcelVerticalAlignment.Top;
                sh.Row(19).Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Center;
                sh.Cells[initialize + 8, 1, initialize + 8, 2].Style.HorizontalAlignment = ExcelRef.Style.ExcelHorizontalAlignment.Left;
                #endregion

                sh.Cells[4, 7, 4, 10].Style.Border.Right.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Column(8).Width = 20;
                sh.Column(9).Width = 15;
                sh.Column(10).Width = 15;
                sh.Column(11).Width = 15;
                sh.Column(1).Style.Font.Bold = true;
                sh.Column(7).Style.Font.Bold = true;
                //sh.Row(23).Style.Font.Bold = true;
                sh.Cells[18,1].Style.Font.Bold = true;
                sh.Row(19).Style.Font.Bold = true;
                sh.Cells[initialize + 1, 1, initialize + 1, 11].Style.Border.Top.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[initialize + 1, 1, initialize + 1, 11].Style.Border.Left.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[initialize + 1, 1, initialize + 1, 11].Style.Border.Right.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                sh.Cells[initialize + 1, 1, initialize + 1, 11].Style.Border.Bottom.Style = ExcelRef.Style.ExcelBorderStyle.Thin;
                var path = System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/" + (type == 1 ? order.id + "_OC_Client_" : "PO_Vendor_") + (type == 1 ? order.client_po_number.Replace("/", "-") : order.po_number.Replace("/", "-")) + ".xlsx");
                Stream stream = File.Create(path);
                excel.SaveAs(stream);
                stream.Close();
                //sendEmail(order.vendorEmail, path);

            }
            catch (Exception ex)
            {
                string createText = ex.Message;
                File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/Logger.txt"), createText + Environment.NewLine);
            }
        }
        public void sendEmail(string toEmail, string attachment)
        {
            Email email = new Email();
            email.Subject = "Order Status";
            email.Body = "Order Successfully placed";
            email.ToEmail = toEmail;
            email.Send(attachment);
        }

        public void MergeWithValue(string range, string value, ExcelRef.ExcelWorksheet sh)
        {
            sh.SelectedRange[range].Merge = true;
            sh.SelectedRange[range].Value = value;
        }
    }
}