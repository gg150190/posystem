﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(POSystem.Startup))]
namespace POSystem
{
    public partial class Startup
    {        
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
