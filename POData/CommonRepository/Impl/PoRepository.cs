﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace POData.CommonRepository.Impl
{
    public class PoRepository<T> : IPoRepository<T> where T : class
    {
        private POMEntities _context;

        public PoRepository()
        {
            _context = new POMEntities();
        }
        private IDbSet<T> _entities;

        public T Find(object id)
        {
            return Entities.Find(id);
        }

        /// <summary>
        /// Used for Add data On database by object
        /// </summary>
        /// <param name="entity">store related data</param>
        public void Add(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                Entities.Add(entity);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }
        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception dbEx)
            {
            }
        }

        /// <summary>
        /// Used for call delete method to search given  Id data on database
        /// </summary>
        /// <param name="id">used for delete data by id</param>
        public void Delete(object id)
        {
            Delete(Find(id));
        }

        /// <summary>
        /// Used for delete entiry from database
        /// </summary>
        /// <param name="entity">store requested entity data for delete</param>
        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                
                Entities.Remove(entity);
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
            }
        }

        /// <summary>
        /// Used for select data from database
        /// </summary>
        public virtual IQueryable<T> All
        {
            get
            {
                return Entities;
            }
        }

        /// <summary>
        /// Used for set Type of Entities
        /// </summary>
        private IDbSet<T> Entities
        {
            get { return (_entities = _context.Set<T>()); }
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            var query = All.Where(predicate);
            return query;
        }

    }
}
