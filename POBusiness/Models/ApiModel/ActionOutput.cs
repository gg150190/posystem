﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ApiModel
{
    public class ActionOutput
    {
    }
    public enum ActionStatus
    {
        Error = 0,
        Successfull = 1,
        LoggedOut = 3,
        Unauthorized = 4
    }
    public class ActionOutputBase
    {
        //  public ActionStatus Status { get; set; }
        public bool Status { get; set; }
        public String Message { get; set; }
        // public List<Object> Results { get; set; }
    }

    public class ActionOutput<T> : ActionOutputBase
    {
        public T Result { get; set; }

        public int Code { get; set; }
    }
}
