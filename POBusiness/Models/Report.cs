﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models
{
    public class Report
    {
        public List<Client> clients { get; set; }
        public List<Vendor> vendors { get; set; }
        public int client_id { get; set; }
        public string quality { get; set; }
        public string gsm { get; set; }
        public int vendorId { get; set; }
        public int type { get; set; }
        public DateTime? fromDate { get; set; }
        public string po_number { get; set; }
        public string rate { get; set; }
        public DateTime? toDate { get; set; }
        public string invoice { get; set; }

    }
}
