﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models
{
    public class Order
    {
        public int id { get; set; }
        public int freight_type { get; set; }
        public string notes { get; set; }
        public string po_number { get; set; }
        public string manual_po_number { get; set; }
        public DateTime? date_added { get; set; }
        public DateTime? date_modified { get; set; }
        public int created_by { get; set; }
        public int modified_by { get; set; }
        public string delivery_address { get; set; }
        public int vendor_id { get; set; }
        public int client_id { get; set; }
        public ICollection<OrderDetail> vendor_po_detail { get; set; }
        public List<OrderDetail> pendingDeliveries { get; set; }
        public string clientname { get; set; }
        public string vendorname { get; set; }
        public string vendorEmail { get; set; }
        public virtual Vendor vendor { get; set; }
        public string statusname { get; set; }
        public bool is_deleted { get; set; }
        public int PaperType { get; set; }
        public string Tin_No { get; set; }
        public string createdByName { get; set; }
        public Client Buyer { get; set; }
        public Client Consignee { get; set; }
        public Client Delivery { get; set; }
        public Int16 status_id { get; set; }
        public string freightName { get; set; }
        public string client_po_number { get; set; }
        public string comments { get; set; }
        public int buyerId { get; set; }
        public int consigneeId { get; set; }
        public int deliveryId { get; set; }

    }

    public class OrderDetail
    {
        public int Id { get; set; }
        public int vendor_po_id { get; set; }
        public int client_po_detail_id { get; set; }
        public short status_id { get; set; }
        public string clientname { get; set; }
        public string vendorname { get; set; }
        public string statusname { get; set; }
        public string paper_length { get; set; }
        public string paper_breadth { get; set; }
        public string bf_grain { get; set; }
        public string quality { get; set; }
        public string gsm { get; set; }
        public decimal quantity { get; set; }
        public decimal ordered_quantity { get; set; }
        public string unitname { get; set; }
        public string rim_weight { get; set; }
        public string size { get; set; }
        public decimal rate { get; set; }
        public string rateType2 { get; set; }
        public DateTime delivery_date { get; set; }
        public short? unit { get; set; }
        public List<InvoiceRemarks> invoice_remarks { get; set; }
        public decimal suppliedQuantitySum { get; set; }
        public string size_name { get; set; }
        public int PaperType { get; set; }
        public string  number_po_ { get; set; }
        public string createdby_ { get; set; }
        public int client_po_id { get; set; }

    }

    public class InvoiceRemarks
    {
        public Int64 id { get; set; }
        public string remarks { get; set; }
        public string supplied_quantity { get; set; }
        public string invoice_number { get; set; }
        public DateTime modified_date { get; set; }
        public int vendor_po_detail_id { get; set; }

    }

    public class StatusAppReject
    {
        public byte status { get; set; }
        public int type { get; set; }
        public List<int> ids { get; set; }
        public string comments { get; set; }
    }

    public class OrderSearch
    {
        public int clientId { get; set; }
        public int paperTypeId { get; set; }
        public string po_number { get; set; }
        public int vendor_id { get; set; }
    }
}
