﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models
{
    public class States
    {
        public int id { get; set; }
        public string state_name { get; set; }
        public string country { get; set; }
    }
}
