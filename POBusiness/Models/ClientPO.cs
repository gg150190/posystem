﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models
{
    public class ClientPO
    {
        public int id { get; set; }
        public int client_id { get; set; }
        public string po_number { get; set; }
        public byte? paper_type { get; set; }
        public int? prefered_vendor_id { get; set; }
        public System.DateTime? date_received { get; set; }
        public byte? status_id { get; set; }
        public System.DateTime date_added { get; set; }
        public System.DateTime date_modified { get; set; }
        public int created_by { get; set; }
        public int modified_by { get; set; }
        public ICollection<ClientPODetail> client_po_detail { get; set; }
        public string clientEmail { get; set; }
        public string clientName { get; set; }
        public string createdByName { get; set; }
        public string vendorName { get; set; }
        public string payment_terms { get; set; }
        public string comments { get; set; }
        public bool isEqual { get; set; }
        public bool isNotValid { get; set; }
        public bool isNotDeleted { get; set; }
    }

    public class ClientPODetail
    {
        public int id { get; set; }
        public int client_po_id { get; set; }
        public string gsm { get; set; }
        public decimal? quantity { get; set; }
        public short? unit { get; set; }
        public decimal? rate { get; set; }
        public byte? rate_type_id { get; set; }
        public decimal? amount { get; set; }
        public System.DateTime? delivery_date { get; set; }
        public byte status_id { get; set; }
        public string quality { get; set; }
        public string paper_length { get; set; }
        public string paper_breadth { get; set; }
        public string size { get; set; }
        public string bf_grain { get; set; }
        public bool IsDeleted { get; set; }
        public decimal ordered_quantity { get; set; }
        public decimal total_ordered_quantity { get; set; }
        public decimal quantity_left { get; set; }
        public string rim_weight { get; set; }
        public int PaperType { get; set; }
        public string unitname { get; set; }
        public string rateType2 { get; set; }
        public string size_name { get; set; }
        public bool isEqual { get; set; }
        public bool is_updated { get; set; }
        public bool is_deleted_modify { get; set; }
        public bool isDashboard { get; set; }

    }

    public class RateType
    {
        public int id { get; set; }
        public string rate_type1 { get; set; }
        public bool is_active { get; set; }
    }

    public class UnitType
    {
        public int id { get; set; }
        public string unit_type1 { get; set; }
        public bool is_active { get; set; }

    }
}
