﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ViewModels
{
    public class Invoice_Order
    {
        public List<Client> clients { get; set; }
        public List<Vendor> vendors { get; set; }

        public List<Order> orders { get; set; }
    }
}
