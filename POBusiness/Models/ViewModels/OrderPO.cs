﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ViewModels
{
    public class OrderPO
    {
        public List<Client> clients { get; set; }
        public List<Vendor> vendors { get; set; }
        public List<RateType> rateTypes { get; set; }
        public List<UnitType> unitTypes { get; set; }
        public List<Freight> freightTypes { get; set; }
    }
}
