﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ViewModels
{
    public class Client_ClientPO
    {
        public List<ClientPO> clientPOs { get; set; }
        public List<Client> clients { get; set; }
        public List<Vendor> vendors { get; set; }
        public List<RateType> rateTypes { get; set; }
        public List<UnitType> unitTypes { get; set; }
    }
}
