﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ViewModels
{
    public class Dashboard_PO_Order
    {
        public List<ClientPO> rejectedPOs { get; set; }
        public List<Order> rejectedOrders { get; set; }
        public List<ClientPO> approvedPOs { get; set; }
        public List<Order> approvedOrders { get; set; }

        public List<RateType> rateTypes { get; set; }
        public List<UnitType> unitTypes { get; set; }
        public dynamic pendingDeliveries { get; set; }
    }
}
