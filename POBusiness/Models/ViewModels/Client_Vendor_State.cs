﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models.ViewModels
{
    public class Client_Vendor_State
    {
        public List<Client> clients { get; set; }
        public List<Vendor> vendors { get; set; }
        public List<States> states { get; set; }
    }
}
