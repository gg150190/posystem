﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Models
{
    public class Client
    {
        public Client()
        {
            city = "";
        }
        public int id { get; set; }
        public string name { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public int state_id { get; set; }
        public string zip { get; set; }
        public string contact_number { get; set; }
        public string pan { get; set; }
        public string client_code { get; set; }
        public string tin_number { get; set; }
        public string ex_number { get; set; }
        public bool is_active { get; set; }
    }
}
