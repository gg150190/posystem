﻿using Ninject.Modules;
using POData.CommonRepository;
using POData.CommonRepository.Impl;
using POBusiness.Models;
using POBusiness.Repository;
using POBusiness.Repository.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POData;

namespace POBusiness.DependencyResolver
{
    public class BusinessLogic : NinjectModule
    {
        public override void Load()
        {
            Bind<IPurchaseOrderRepository>().To<PurchaseOrderRepository>().InSingletonScope();
            Bind<IClientRepository>().To<ClientRepository>().InSingletonScope();
            Bind<IClientPORepository>().To<ClientPORepository>().InTransientScope();
            Bind<IInvoiceRepository>().To<InvoiceRepository>().InTransientScope();
            Bind<IVendorRepository>().To<VendorRepository>().InSingletonScope();
            Bind<IReportRepository>().To<ReportRepository>().InTransientScope();
            Bind<IPoRepository<client_po>>().To<PoRepository<client_po>>().InTransientScope();
            Bind<IPoRepository<client_po_detail>>().To<PoRepository<client_po_detail>>().InTransientScope();
            Bind<IPoRepository<client>>().To<PoRepository<client>>().InSingletonScope(); 
            Bind<IPoRepository<vendor>>().To<PoRepository<vendor>>().InSingletonScope();
            Bind<IPoRepository<state>>().To<PoRepository<state>>().InSingletonScope();
            Bind<IPoRepository<rate_type>>().To<PoRepository<rate_type>>().InSingletonScope();
            Bind<IPoRepository<unit_type>>().To<PoRepository<unit_type>>().InSingletonScope();
            Bind<IPoRepository<vendor_po>>().To<PoRepository<vendor_po>>().InTransientScope();
            Bind<IPoRepository<vendor_po_detail>>().To<PoRepository<vendor_po_detail>>().InTransientScope();
            Bind<IPoRepository<invoice_remarks>>().To<PoRepository<invoice_remarks>>().InTransientScope();
            Bind < IPoRepository<freight>>().To<PoRepository<freight>>().InSingletonScope();
        }
    }
}