﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository.Common
{
    public interface IRepository<T> where T : class
    {
        T Find(object id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(object id);
        List<T> All { get; }
        List<T> FindBy(Expression<Func<T, bool>> predicate);
    }
}
