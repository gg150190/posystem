﻿using POBusiness.Models;
using POBusiness.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using POData.CommonRepository;
using POData;
using AutoMapper;
using POBusiness.Enums;

namespace POBusiness.Repository.Impl
{
    public class InvoiceRepository : IInvoiceRepository
    {
        IPoRepository<vendor_po> _repository;

        public InvoiceRepository(IPoRepository<vendor_po> repository)
        {
            _repository = repository;
        }

        public List<Invoice> All
        {
            get
            {
                var res = _repository.All;
                return Mapper.Map<List<Invoice>>(res.ToList());
            }
        }
        public List<Order> AllOrders
        {
            get
            {
                var res = _repository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
                return Mapper.Map<List<Order>>(res.ToList());
            }
        }

        public void Add(Invoice entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(Invoice entity)
        {
            throw new NotImplementedException();
        }

        public Invoice Find(object id)
        {
            throw new NotImplementedException();
        }

        public Order FindByOrderId(object id)
        {
            var res = _repository.Find(id);
            return Mapper.Map<Order>(res);
        }

        public List<Invoice> FindBy(Expression<Func<Invoice, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Update(Invoice entity)
        {
            throw new NotImplementedException();
        }
        public void RemoveCompleted(string deleteStr)
        {
            var ids = deleteStr.Split(',').Select(Int32.Parse).ToList();
            foreach (var item in ids)
            {
                var res = _repository.Find(item);
                res.is_deleted = true;
                _repository.Update(res);
            }
        }
    }
}
