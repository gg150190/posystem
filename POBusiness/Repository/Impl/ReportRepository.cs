﻿using POBusiness.Enums;
using POBusiness.Models;
using POData;
using POData.CommonRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository.Impl
{
    public class ReportRepository : IReportRepository
    {
        IPoRepository<client_po> _repository;
        IPoRepository<client_po_detail> _clientPORepository;
        IPoRepository<vendor_po> _orderPORepository;
        IPoRepository<vendor_po_detail> _orderDetailRepository;
        IPoRepository<invoice_remarks> _invoiceRemarks;

        public ReportRepository(IPoRepository<client_po> repository, IPoRepository<client_po_detail> clientPORepository, IPoRepository<vendor_po> orderPORepository, IPoRepository<vendor_po_detail> orderDetailRepository, IPoRepository<invoice_remarks> invoiceRemarks)
        {
            _repository = repository;
            _clientPORepository = clientPORepository;
            _orderPORepository = orderPORepository;
            _orderDetailRepository = orderDetailRepository;
            _invoiceRemarks = invoiceRemarks;
        }
        public dynamic SearchReports(Report report)
        {
            try
            {

                if (report.type == 1)
                {
                    var res =
                              from m in _repository.All.ToList()
                              join n in _clientPORepository.All.ToList() on m.id equals n.client_po_id
                              join p in _orderDetailRepository.All.ToList() on n.id equals p.client_po_detail_id

                              where (string.IsNullOrEmpty(Convert.ToString(report.po_number)) || m.po_number.Contains(report.po_number))
                              && (report.client_id == 0 || m.client_id == report.client_id)
                              && (report.quality == null || n.quality.Contains(report.quality))
                              && (string.IsNullOrEmpty(Convert.ToString(report.rate)) || Convert.ToString(n.rate) == (Convert.ToString(report.rate)))
                              && (string.IsNullOrEmpty(Convert.ToString(report.gsm)) || n.gsm == (report.gsm.ToString()))
                              && (string.IsNullOrEmpty(Convert.ToString(report.invoice)) || string.Join(",", p.invoice_remarks.ToList().Select(t => t.invoice_number)).ToLower().Contains(report.invoice.ToLower().ToString()))
                              && (report.vendorId == 0 || m.prefered_vendor_id == report.vendorId)
                              // && (!string.IsNullOrEmpty(Convert.ToString(report.fromDate)) && !string.IsNullOrEmpty(Convert.ToString(report.toDate))) || (m.date_modified.Date >= report.fromDate && m.date_modified <= report.toDate)
                              && (string.IsNullOrEmpty(Convert.ToString(report.toDate)) || (m.date_modified.Date <= Convert.ToDateTime(report.toDate).Date))
                              && (string.IsNullOrEmpty(Convert.ToString(report.fromDate)) || (m.date_modified.Date >= Convert.ToDateTime(report.fromDate).Date))
                              && (p.vendor_po.status_id == (byte)Status.Approved || p.vendor_po.status_id == (byte)Status.Completed || p.vendor_po.status_id == (byte)Status.Partial)
                              select new
                              {
                                  id = p.Id,
                                  client = m.client.name,
                                  quality = n.quality,
                                  gsm = n.gsm == null ? "" : n.gsm,
                                  rate = n.rate,
                                  size = n.size,
                                  number = m.po_number,
                                  unit = n.unit_type.unit_type1,
                                  vendor_PO = p.vendor_po.po_number,
                                  vendor = m.vendor == null ? "" : m.vendor.name,
                                  orderedQuantity = p.ordered_quantity,
                                  supplied_quantity = p.invoice_remarks.ToList().Sum(t => Convert.ToDouble(t.supplied_quantity)),
                                  invoice_no = string.Join(",", p.invoice_remarks.ToList().Select(t => t.invoice_number))
                              };

                    return res.Distinct().ToList();
                }
                else
                {
                    var res = from m in _orderPORepository.All.ToList()
                              join n in _orderDetailRepository.All.ToList() on m.id equals n.vendor_po_id
                              where
                                 (string.IsNullOrEmpty(Convert.ToString(report.po_number)) || m.po_number.Contains(report.po_number))
                              && (report.client_id == 0 || m.client_id == report.client_id)
                              && (report.quality == null || n.client_po_detail.quality.Contains(report.quality))
                              && (string.IsNullOrEmpty(Convert.ToString(report.rate)) || Convert.ToString(n.client_po_detail.rate) == (Convert.ToString(report.rate)))
                              && (string.IsNullOrEmpty(Convert.ToString(report.gsm)) || n.client_po_detail.gsm == (report.gsm.ToString()))
                              && (string.IsNullOrEmpty(Convert.ToString(report.invoice)) || string.Join(",", n.invoice_remarks.ToList().Select(t => t.invoice_number)).ToLower().Contains(report.invoice.ToLower().ToString()))
                              && (report.vendorId == 0 || m.vendor_id == report.vendorId)
                              //&& (!string.IsNullOrEmpty(Convert.ToString(report.fromDate)) && !string.IsNullOrEmpty(Convert.ToString(report.toDate))) || (m.date_modified.Date >= report.fromDate && m.date_modified <= report.toDate)
                                 && (string.IsNullOrEmpty(Convert.ToString(report.toDate)) || (m.date_modified.Date <= Convert.ToDateTime(report.toDate).Date))
                              && (string.IsNullOrEmpty(Convert.ToString(report.fromDate)) || (m.date_modified.Date >= Convert.ToDateTime(report.fromDate).Date))
                              && (n.vendor_po.status_id == (byte)Status.Approved || n.vendor_po.status_id == (byte)Status.Completed || n.vendor_po.status_id == (byte)Status.Partial)

                              select new
                              {
                                  id = n.Id,
                                  client = m.client.name,
                                  quality = n.client_po_detail.quality,
                                  gsm = n.client_po_detail.gsm,
                                  rate = n.client_po_detail.rate,
                                  size = n.client_po_detail.size,
                                  number = n.client_po_detail.client_po.po_number,
                                  unit = n.client_po_detail.unit_type.unit_type1,
                                  vendor_PO = m.po_number,
                                  vendor = m.vendor == null ? "" : m.vendor.name,
                                  orderedQuantity = n.ordered_quantity,
                                  supplied_quantity = n.invoice_remarks.ToList().Sum(t => Convert.ToDouble(t.supplied_quantity)),
                                  invoice_no = string.Join(",", n.invoice_remarks.ToList().Select(t => t.invoice_number))
                              };
                    return res.ToList().Distinct();
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }


    }
}
