﻿using POBusiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using POData.CommonRepository;
using POData;
using AutoMapper;
using POBusiness.Enums;

namespace POBusiness.Repository.Impl
{
    public class ClientPORepository : IClientPORepository
    {
        IPoRepository<client_po> _repository;
        IPoRepository<client_po_detail> _clientPORepository;
        IPoRepository<rate_type> _rateTypePORepository;
        IPoRepository<unit_type> _unitTypePORepository;
        IPoRepository<freight> _freightTypePORepository;
        IPoRepository<vendor_po> _orderPORepository;
        IPoRepository<vendor_po_detail> _orderDetailRepository;
        IPoRepository<invoice_remarks> _invoice_remarks;
        public ClientPORepository(IPoRepository<client_po> repository, IPoRepository<client_po_detail> clientPORepository, IPoRepository<rate_type> rateTypePORepository, IPoRepository<unit_type> unitTypePORepository, IPoRepository<vendor_po> orderPORepository, IPoRepository<freight> freightTypePORepository, IPoRepository<vendor_po_detail> orderDetailRepository, IPoRepository<invoice_remarks> invoice_remarks)
        {
            _repository = repository;
            _clientPORepository = clientPORepository;
            _rateTypePORepository = rateTypePORepository;
            _unitTypePORepository = unitTypePORepository;
            _orderPORepository = orderPORepository;
            _orderDetailRepository = orderDetailRepository;
            _freightTypePORepository = freightTypePORepository;
            _invoice_remarks = invoice_remarks;
        }

        public List<ClientPO> All
        {
            get
            {
                var res = _repository.All.Where(t => t.is_deleted != true).OrderByDescending(t => t.id);
                return Mapper.Map<List<ClientPO>>(res.ToList());
            }
        }

        public void Add(ClientPO entity)
        {
            entity.date_added = DateTime.UtcNow;
            entity.date_modified = DateTime.UtcNow;
            entity.status_id = 1;
            int val1 = 144;
            if (entity.vendorName != null && entity.vendorName.ToLower().Contains("emami paper mills"))
                val1 = 100;

            foreach (var detail in entity.client_po_detail)
            {
                if (entity.paper_type == 2)
                {
                    double result = 0;
                    if (detail.size == "1")
                        result = (Convert.ToDouble(detail.paper_length) * Convert.ToDouble(detail.paper_breadth) * Convert.ToDouble(detail.gsm) * val1 * 6.45) / 10000000;
                    else
                        result = (Convert.ToDouble(detail.paper_length) * Convert.ToDouble(detail.paper_breadth) * Convert.ToDouble(detail.gsm) * val1) / 10000000;

                    if (result > 32)
                        detail.rim_weight = Convert.ToString(result / 2);
                    else
                        detail.rim_weight = Convert.ToString(result);
                }
                detail.status_id = 1;
                detail.ordered_quantity = 0;
            }
            var data = Mapper.Map<client_po>(entity);
            _repository.Add(data);
            entity.id = data.id;
            int count = 0;
            foreach (var item in data.client_po_detail)
            {
                entity.client_po_detail.ToList()[count].id = item.id;
                count++;
            }
            //sendEmail(entity.clientEmail);
            //email.GmailUsername=entity.
        }

        //public void sendEmail(string toEmail)
        //{
        //    Email email = new Email();
        //    email.Subject = "Order Status";
        //    email.Body = "Order Successfully placed";
        //    email.ToEmail = toEmail;
        //    email.Send();
        //}
        public bool DeleteClientPO_(object id)
        {
            var res = _repository.Find(id);
            var intId = Convert.ToInt32(id);
            var po_details = _clientPORepository.FindBy(t => t.client_po_id == intId).ToList();
            if (DeleteVendorPos(po_details.Select(t => t.id).ToList()) == false)
                return false;
            else
            {
                res.status_id = (byte)Status.Updated;
                _repository.Update(res);
                foreach (var item in po_details)
                {
                    item.status_id = (byte)Status.UpdateDeleted;
                    item.is_updated = true;
                    _clientPORepository.Update(item);
                }
                //res.is_deleted = true;
                //_repository.Update(res);
                //if (po_details != null && po_details.Count() > 0)
                //{
                //    foreach (var item in po_details)
                //    {
                //        item.is_deleted = true;
                //        _clientPORepository.Update(item);
                //        //UpdateVendorPos(item.id);
                //    }
                //}
            }
            return true;
        }
        public void Delete(object id)
        {

        }

        public void Delete(ClientPO entity)
        {
            throw new NotImplementedException();
        }

        public ClientPO Find(object id)
        {
            var res = _repository.Find(id);
            return Mapper.Map<ClientPO>(res);
        }

        public List<ClientPO> FindBy(Expression<Func<ClientPO, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public List<ClientPODetail> FindByClientId(int clientId, int paperTypeId, string po_number, int vendorId)
        {
            var res = (from c in _repository.All.ToList()
                       join l in _clientPORepository.All.ToList() on c.id equals l.client_po_id
                       where c.client_id == clientId && c.paper_type == paperTypeId && l.ordered_quantity < l.quantity
                       && l.is_deleted != true && c.is_deleted != true && (c.status_id == (byte)Status.Approved || c.status_id == (byte)Status.Updated)
                       && c.po_number == po_number && (vendorId == 0 || c.prefered_vendor_id == vendorId) && l.is_updated != true
                       select l).ToList();

            return Mapper.Map<List<ClientPODetail>>(res);
        }
        public List<string> GetAllPONumbers(int clientId)
        {
            var res = (from c in _repository.All.ToList()
                       join l in _clientPORepository.All.ToList() on c.id equals l.client_po_id
                       where c.client_id == clientId && l.ordered_quantity < l.quantity
                       && l.is_deleted != true && c.is_deleted != true && (c.status_id == (byte)Status.Approved|| c.status_id == (byte)Status.Updated)
                       select c.po_number).Distinct().ToList();

            return res;
        }
        public void Update(ClientPO entity)
        {
            if (entity.isEqual == false)
            {
                entity.status_id = (byte)Status.Updated;
                var lst = entity.client_po_detail.Where(t => t.IsDeleted == true).ToList();
                List<int> details = new List<int>();
                if (lst.Count() > 0)
                    details = lst.Select(t => t.id).ToList();

                if (DeleteVendorPos(details) == true)
                {
                    foreach (var item in entity.client_po_detail)
                    {
                        if (item.total_ordered_quantity > 0)
                        {
                            if (!chkValidSuppliedQnty(item, item.quantity))
                            {
                                entity.isNotValid = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    entity.isNotDeleted = true;
                    return;
                }

                var res = Mapper.Map<client_po>(entity);
                res.client_po_detail = null;
                //res.status_id = (byte)Status.NotStarted;
                _repository.Update(res);
                int val1 = 144;
                if (entity.vendorName != null && entity.vendorName.ToLower().Contains("emami paper mills"))
                    val1 = 100;
                foreach (var item in entity.client_po_detail)
                {
                    item.client_po_id = res.id;
                    if (item.IsDeleted)
                        DeleteClientPODetail(item.id);
                    else
                    {
                        if (entity.paper_type == 2)
                        {
                            double result = 0;
                            if (item.size == "1")
                                result = (Convert.ToDouble(item.paper_length) * Convert.ToDouble(item.paper_breadth) * Convert.ToDouble(item.gsm) * val1 * 6.45) / 10000000;
                            else
                                result = (Convert.ToDouble(item.paper_length) * Convert.ToDouble(item.paper_breadth) * Convert.ToDouble(item.gsm) * val1) / 10000000;

                            if (result > 32)
                                item.rim_weight = Convert.ToString(result / 2);
                            else
                                item.rim_weight = Convert.ToString(result);
                        }
                        AddUpdateClientPODetail(item);
                        //if (item.ordered_quantity > 0)
                        //    UpdateVendorPos(item.id);
                    }

                }
            }
        }
        public void UpdateVendorPos(Int32 id)
        {
            var vendorPODetailList = _orderDetailRepository.FindBy(t => t.client_po_detail_id == id && t.is_deleted != true).ToList();
            var lastClientPODetail_ = _clientPORepository.Find(id);
            if (vendorPODetailList != null && vendorPODetailList.Count() > 0)
            {
                foreach (var item1 in vendorPODetailList)
                {
                    var invoiceDetails = _invoice_remarks.FindBy(t => t.vendor_po_detail_id == item1.Id).ToList();
                    string bodyText = "";
                    string subject = "";
                    bodyText = "PO has been updated.";
                    subject = "PO Updated";
                    item1.is_deleted = true;
                    _orderDetailRepository.Update(item1);

                    if (invoiceDetails.Count() == 0)
                        lastClientPODetail_.ordered_quantity = lastClientPODetail_.ordered_quantity - item1.ordered_quantity;
                    else
                    {
                        var total = invoiceDetails.Sum(t => Convert.ToDecimal(t.supplied_quantity));
                        lastClientPODetail_.ordered_quantity = (lastClientPODetail_.ordered_quantity - item1.ordered_quantity) + total;
                    }

                    sendCommonEmailContent(item1, bodyText, subject);
                }
            }
            lastClientPODetail_.status_id = lastClientPODetail_.status_id == (byte)Status.UpdateDeleted ? lastClientPODetail_.status_id : (byte)Status.NotStarted;
            lastClientPODetail_.is_deleted = lastClientPODetail_.status_id == (byte)Status.UpdateDeleted ? true : lastClientPODetail_.is_deleted;
            lastClientPODetail_.is_updated = false;
            _clientPORepository.Update(lastClientPODetail_);
        }
        public bool DeleteVendorPos(List<int> details)
        {
            if (details.Count() == 0)
                return true;
            bool isValid = true;
            List<vendor_po_detail> vendorPODetailList = new List<vendor_po_detail>();
            foreach (var item in details)
            {
                vendorPODetailList = _orderDetailRepository.FindBy(t => t.client_po_detail_id == item && t.is_deleted != true).ToList();
                var vendorPODetailList_ = vendorPODetailList.Select(t => Convert.ToInt32(t.Id)).ToList();
                var invoiceDetails_ = _invoice_remarks.FindBy(x => x.vendor_po_detail_id != null && vendorPODetailList_.Contains((int)x.vendor_po_detail_id)).ToList();
                if (invoiceDetails_.Count() > 0)
                    isValid = false;
            }
            if (isValid == false)
                return isValid;
            //else
            //{
            //    foreach (var item1 in vendorPODetailList)
            //    {
            //        var invoiceDetails = _invoice_remarks.FindBy(t => t.vendor_po_detail_id == item1.Id).ToList();
            //        string bodyText = "PO has been deleted";
            //        string subject = "PO Deleted";
            //        if (invoiceDetails.Count() == 0)
            //        {
            //            item1.is_deleted = true;
            //            _orderDetailRepository.Update(item1);
            //            var lastClientPODetail_ = _clientPORepository.Find(item1.client_po_detail_id);
            //            lastClientPODetail_.ordered_quantity = lastClientPODetail_.ordered_quantity - item1.ordered_quantity;
            //            _clientPORepository.Update(lastClientPODetail_);
            //            sendCommonEmailContent(item1, bodyText, subject);
            //        }
            //    }
            //}
            return true;
        }


        public void sendCommonEmailContent(vendor_po_detail item1, string bodyText, string subject)
        {
            var paperType = item1.client_po_detail.client_po.paper_type;
            string name = paperType == 1 ? "Sharvi Traders Pvt Ltd" : "Sharvi Papers Pvt Ltd";
            var body = "Dear Sir," + Environment.NewLine + bodyText + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                          + "Thanks & Regards" + Environment.NewLine + Environment.NewLine + paperType + Environment.NewLine + "Plot No. 193, Street No.2," + Environment.NewLine +
                          "Krishna Colony, Opp. Sector-25," + Environment.NewLine + "Faridabad-121004 (Haryana)" + Environment.NewLine + "Website : www.sharvipapers.com";
            sendEmail(item1.vendor_po.vendor.email, "", item1.vendor_po.po_number, body, "PO- " + item1.vendor_po.po_number);
            var client_po_number = item1.client_po_detail.client_po.po_number;
            sendEmail(item1.vendor_po.client.email, "", client_po_number, body, "PO- " + client_po_number);
        }
        public bool AddUpdateClientPODetail(ClientPODetail detail, bool isUpdate = false)
        {
            var data = Mapper.Map<client_po_detail>(detail);
            if (detail.id == 0)
            {
                detail.status_id = (byte)Status.NotStarted;
                _clientPORepository.Add(data);
            }
            else if (detail.isEqual == false && detail.isDashboard == false)
            {
                var oldentry = _clientPORepository.Find(detail.id);
                oldentry.ordered_quantity = detail.total_ordered_quantity;
                if (!chkValidSuppliedQnty(detail, detail.quantity))
                    return false;
                Mapper.Map(detail, oldentry);
                detail.is_updated = true;
                oldentry.is_updated = true;
                _clientPORepository.Update(oldentry);

                var oldClientPO = _repository.Find(oldentry.client_po.id);
                if (oldClientPO.status_id != (byte)Status.RejectUpdated)
                    oldClientPO.status_id = (byte)Status.Updated;
                _repository.Update(oldClientPO);
            }
            else
            {
                var oldentry = _clientPORepository.Find(detail.id);
                Mapper.Map(detail, oldentry);
                _clientPORepository.Update(oldentry);
            }
            detail.id = data.id;
            return true;
        }

        public bool chkValidSuppliedQnty(ClientPODetail detail, decimal? quantity = 0)
        {
            if (detail.total_ordered_quantity > 0)
            {
                var vendorPODetailList_ = _orderDetailRepository.FindBy(t => t.client_po_detail_id == detail.id).ToList();
                var vendorPODetailList = vendorPODetailList_.Select(t => Convert.ToInt32(t.Id)).ToList();
                var invoiceDetails = _invoice_remarks.FindBy(x => x.vendor_po_detail_id != null && vendorPODetailList.Contains((int)x.vendor_po_detail_id)).ToList();
                if (invoiceDetails.Count() > 0 && invoiceDetails.Sum(t => Convert.ToDecimal(t.supplied_quantity)) > quantity)
                    return false;
                //else
                //    UpdateVendorPos(detail.id);
            }
            return true;
        }

        public bool DeleteClientPODetail(object id)
        {
            var res = _clientPORepository.Find(id);
            if (DeleteVendorPos(new List<int>() { res.id }) == false)
                return false;
            else
            {
                var client_po = _repository.Find(res.client_po_id);
                client_po.status_id = (byte)Status.Updated;
                _repository.Update(client_po);
                res.status_id = (byte)Status.UpdateDeleted;
                res.is_updated = true;
                _clientPORepository.Update(res);
            }
            return true;
        }
        public List<RateType> AllRateTypes
        {
            get
            {
                var res = _rateTypePORepository.All;
                return Mapper.Map<List<RateType>>(res.ToList());
            }
        }
        public List<UnitType> AllUnitTypes
        {
            get
            {
                var res = _unitTypePORepository.All;
                return Mapper.Map<List<UnitType>>(res.ToList());
            }
        }

        public int CreateOrder(Order order)
        {
            foreach (var item in order.vendor_po_detail)
            {
                item.client_po_detail_id = item.Id;
                item.Id = 0;
                item.status_id = (byte)Status.Ordered;

                var oldentry = _clientPORepository.Find(item.client_po_detail_id);
                oldentry.status_id = (byte)Status.Ordered;
                oldentry.ordered_quantity += item.ordered_quantity;
                _clientPORepository.Update(oldentry);

                //var oldClientPO = _repository.Find(oldentry.client_po_id);
                //oldClientPO.status_id = (byte)Status.Partial;
                //_repository.Update(oldClientPO);
            }

            order.date_added = DateTime.UtcNow;
            order.status_id = (byte)Status.NotStarted;
            order.date_modified = DateTime.UtcNow;
            var res = Mapper.Map<vendor_po>(order);
            //if (order.vendor_po_detail != null && order.vendor_po_detail.Count() > 0)
            //    res.client_po_id = order.vendor_po_detail.FirstOrDefault().client_po_id;
            res.vendor = null;
            _orderPORepository.Add(res);
            return res.id;
            //sendEmail(order.vendorEmail);
        }

        public void UpdateOrder(OrderDetail detail)
        {
            var totalSupplied = detail.invoice_remarks.Sum(t => Convert.ToDecimal(t.supplied_quantity));
            if (totalSupplied > 0 && totalSupplied < detail.quantity)
                detail.status_id = (byte)Status.Partial;
            else if (totalSupplied >= detail.quantity)
                detail.status_id = (byte)Status.Completed;
            else
                detail.status_id = (byte)Status.Ordered;

            var res = Mapper.Map<vendor_po_detail>(detail);
            res.invoice_remarks = null;
            _orderDetailRepository.Update(res);
            detail.suppliedQuantitySum = totalSupplied;
            if (detail.invoice_remarks != null && detail.invoice_remarks.Count() > 0)
            {
                foreach (var item in detail.invoice_remarks)
                {
                    item.modified_date = DateTime.UtcNow;
                    if (item.id > 0)
                        _invoice_remarks.Update(Mapper.Map<invoice_remarks>(item));

                    else
                    {
                        item.vendor_po_detail_id = detail.Id;
                        var invoice = Mapper.Map<invoice_remarks>(item);
                        _invoice_remarks.Add(invoice);
                        item.id = invoice.id;
                    }
                }
            }

            var order = _orderPORepository.FindBy(t => t.id == detail.vendor_po_id).FirstOrDefault();
            var totalOrders = _orderDetailRepository.FindBy(t => t.client_po_detail_id == detail.client_po_detail_id).ToList();
            var totalCount = totalOrders.Where(t => t.status_id == (byte)Status.Completed).Count();
            var oldentry = _clientPORepository.Find(detail.client_po_detail_id);
            if (totalOrders.Count() != totalCount)
            {
                oldentry.status_id = (byte)Status.Partial;
                if (order != null)
                {
                    order.status_id = (byte)Status.Partial;
                }
            }
            else
            {
                oldentry.status_id = (byte)Status.Completed;
                if (order != null)
                {
                    order.status_id = (byte)Status.Completed;
                }
            }
            _clientPORepository.Update(oldentry);
            _orderPORepository.Update(order);

        }

        public void UpdateRejectedOrder(OrderDetail detail)
        {
            decimal newQuantity = 0;
            var order = _orderDetailRepository.FindBy(t => t.Id == detail.Id).FirstOrDefault();
            if (order.ordered_quantity != null && order.ordered_quantity > detail.quantity)
                newQuantity = -(Convert.ToDecimal(order.ordered_quantity) - detail.quantity);
            else
                newQuantity = detail.quantity - Convert.ToInt32(order.ordered_quantity);

            order.ordered_quantity = detail.quantity;
            _orderDetailRepository.Update(order);
            var oldentry = _clientPORepository.Find(detail.client_po_detail_id);
            oldentry.ordered_quantity += newQuantity;
            _clientPORepository.Update(oldentry);
        }

        public void CompleteRejectedOrder(int id, int type)
        {
            if (type == 1)
            {
                var order1 = _orderPORepository.FindBy(t => t.id == id).FirstOrDefault();
                order1.status_id = (byte)Status.NotStarted;
                _orderPORepository.Update(order1);
            }
            else
            {
                var order1 = _repository.FindBy(t => t.id == id).FirstOrDefault();
                order1.status_id = order1.status_id == (byte)Status.Rejected ? (byte)Status.NotStarted : (byte)Status.Updated;
                var client_po_details = _clientPORepository.FindBy(t => t.client_po_id == order1.id).ToList();
                foreach (var item in client_po_details)
                {
                    if (item.is_updated == true)
                    {
                        item.status_id = (byte)Status.NotStarted;
                        _clientPORepository.Update(item);
                    }
                }
                _repository.Update(order1);
            }
        }
        public void DeleteRejectedOrder(OrderDetail detail)
        {
            var order = _orderDetailRepository.FindBy(t => t.Id == detail.Id).FirstOrDefault();
            order.is_deleted = true;
            _orderDetailRepository.Update(order);
            var oldentry = _clientPORepository.Find(detail.client_po_detail_id);
            oldentry.ordered_quantity -= detail.quantity;
            _clientPORepository.Update(oldentry);
        }
        public void MarkAsComplete(int orderId)
        {
            var res = _orderPORepository.Find(orderId);
            res.status_id = (byte)Status.Completed;
            foreach (var detail in res.vendor_po_detail)
            {
                detail.status_id = (byte)Status.Completed;
                var oldentry = _clientPORepository.Find(detail.client_po_detail_id);
                oldentry.status_id = (byte)Status.Completed;
                _clientPORepository.Update(oldentry);
            }
            _orderPORepository.Update(res);
        }
        public List<Freight> GetFreights
        {
            get
            {
                var res = _freightTypePORepository.All;
                return Mapper.Map<List<Freight>>(res.ToList());
            }
        }

        public List<ClientPO> GetRejectedPOs()
        {
            var res = _repository.All.Where(t => (t.status_id == (byte)Status.Rejected || t.status_id == (byte)Status.RejectUpdated) && t.is_deleted != true).OrderByDescending(t => t.id);
            var result = Mapper.Map<List<ClientPO>>(res.ToList());
            result.ForEach(t => t.client_po_detail = (t.status_id == (byte)Status.RejectUpdated) ? t.client_po_detail.Where(y => y.status_id == (byte)Status.RejectUpdated).ToList() : t.client_po_detail);
            return result;
        }

        public List<Order> GetRejectedOrders()
        {
            var res = _orderPORepository.All.Where(t => t.status_id == (byte)Status.Rejected && t.is_deleted != true).OrderByDescending(t => t.id);
            return Mapper.Map<List<Order>>(res.ToList());
        }
        public List<Order> GetApprovedOrders()
        {
            var res = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Completed && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
            return Mapper.Map<List<Order>>(res.ToList());
        }
        public List<ClientPO> GetApprovedPO()
        {
            var res = _repository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Completed && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
            return Mapper.Map<List<ClientPO>>(res.ToList());
        }


        //public List<Order> GetPendingDeliveries()
        //{
        //    var res = _orderPORepository.All.Where(t => t.status_id != (byte)Status.Completed && t.is_deleted != true).OrderByDescending(t => t.id);
        //    return Mapper.Map<List<Order>>(res.ToList());
        //}

        public dynamic GetPendingDeliveries()
        {
            List<OrderDetail> details = new List<OrderDetail>();
            var res = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Completed && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
            var newRes = Mapper.Map<List<Order>>(res.ToList()).Where(t => t.pendingDeliveries.Count() > 0).ToList();
            newRes.ForEach(r => r.pendingDeliveries.ForEach(s => { s.number_po_ = r.po_number; s.createdby_ = r.createdByName; }));
            var finalList = newRes.Select(t => t.pendingDeliveries).ToList();
            foreach (var item in finalList)
            {
                details.AddRange(item);
            }
            return details.Distinct().ToList();
            //var res = from m in _orderPORepository.All.ToList()
            //          join n in _orderDetailRepository.All.ToList() on m.id equals n.Id
            //          join r in _clientPORepository.All.ToList() on n.client_po_detail_id equals r.id
            //          where m.is_deleted != true && m.status_id != (byte)Status.Completed && m.status_id != (byte)Status.Rejected && m.status_id != (byte)Status.NotStarted && Convert.ToDateTime(r.delivery_date).Date < DateTime.Now.Date
            //          orderby m.id
            //          select new
            //          {
            //              PO_Number = m.po_number,
            //              Client = m.client.name,
            //              VendorName = m.vendor.name,
            //              DeliveryDate = r.delivery_date,
            //              CreatedBy = Convert.ToString(m.AspNetUser1.FirstName) == null ? "" : m.AspNetUser1.FirstName + Convert.ToString(m.AspNetUser1.LastName) == null ? "" : m.AspNetUser1.LastName
            //          };

            //return res.ToList();
        }

        /// <summary>
        /// For Mobile API's
        /// </summary>
        /// <returns></returns>

        #region Mobile API's

        public dynamic GetDetailsCount()
        {
            var pendingPO = _repository.All.Where(t => t.is_deleted != true && (t.status_id == (byte)Status.NotStarted || t.status_id == (byte)Status.Updated)).Count();
            var pendingOrders = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id == (byte)Status.NotStarted).Count();
            var res = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Completed && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
            var newRes = Mapper.Map<List<Order>>(res.ToList()).Where(t => t.pendingDeliveries.Count() > 0);
            var pendingDeliveries = newRes.Count();
            return new { pendingPO = pendingPO, pendingOrders = pendingOrders, pendingDeliveries = pendingDeliveries };
        }
        public dynamic GetPODetails(int status, int total_records, int page_number)
        {
            if (status == 1)
            {
                var res = _repository.All.Where(t => t.is_deleted != true && (t.status_id == (byte)Status.NotStarted || t.status_id == (byte)Status.Updated))
                    .OrderByDescending(t => t.id).Skip(total_records * (page_number - 1)).Take(total_records);
                return Mapper.Map<List<ClientPO>>(res.ToList()).Select(t => new { Id = t.id, PO_Number = t.po_number, StatusId = t.status_id, Client = t.clientName, CreatedBy = t.createdByName, Items = t.status_id == (byte)Status.Updated ? t.client_po_detail.Where(r => r.is_updated == true && (r.status_id == (byte)Status.NotStarted || r.status_id == (byte)Status.UpdateDeleted || r.status_id == (byte)Status.Ordered || r.status_id == (byte)Status.Partial || r.status_id == (byte)Status.Completed)) : t.client_po_detail });
            }
            else if (status == 2)
            {
                var res = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id == (byte)Status.NotStarted).OrderByDescending(t => t.id).Skip(total_records * (page_number - 1)).Take(total_records);
                return Mapper.Map<List<Order>>(res.ToList()).Select(t => new { Id = t.id, PO_Number = t.po_number, Client = t.clientname, CreatedBy = t.createdByName, Items = t.vendor_po_detail });
            }
            else
            {
                var res = _orderPORepository.All.Where(t => t.is_deleted != true && t.status_id != (byte)Status.Completed && t.status_id != (byte)Status.Rejected && t.status_id != (byte)Status.NotStarted).OrderByDescending(t => t.id);
                var newRes = Mapper.Map<List<Order>>(res.ToList()).Where(t => t.pendingDeliveries.Count() > 0).Skip(total_records * (page_number - 1)).Take(total_records);
                return Mapper.Map<List<Order>>(newRes.ToList()).Select(t => new { Id = t.id, PO_Number = t.po_number, Client = t.clientname, CreatedBy = t.createdByName, Items = t.pendingDeliveries });
                //IEnumerable<dynamic> res = GetPendingDeliveries();
                //return res.ToList().Skip(total_records * (page_number - 1)).Take(total_records);
            }
        }

        public void UpdateStatus(StatusAppReject status)
        {
            try
            {

                foreach (var id in status.ids)
                {
                    if (status.type == 1)
                    {
                        var res = _repository.Find(id);
                        res.comments = status.comments;

                        var details = res.client_po_detail;
                        if (res.status_id == (byte)Status.Updated)
                        {
                            if (status.status == (byte)Status.Approved)
                            {
                                foreach (var item in details)
                                {
                                    UpdateVendorPos(item.id);
                                }
                            }
                            else
                            {
                                foreach (var item in details)
                                {
                                    if (item.is_updated == true)
                                    {
                                        //var detail_ = _clientPORepository.Find(item.id);
                                        if (item.status_id == (byte)Status.UpdateDeleted)
                                        {
                                            item.status_id = (byte)Status.NotStarted;
                                            item.is_updated = false;
                                        }
                                        else
                                            item.status_id = (byte)Status.RejectUpdated;
                                        _clientPORepository.Update(item);
                                    }
                                }
                            }
                        }
                        //res.status_id = status.status;
                        res.status_id = res.status_id == (byte)Status.Updated ? (status.status == (byte)Status.Approved ? status.status : (byte)Status.RejectUpdated) : status.status;
                        _repository.Update(res);

                    }
                    else
                    {
                        var res = _orderPORepository.Find(id);
                        res.status_id = status.status;
                        res.comments = status.comments;
                        _orderPORepository.Update(res);
                        byte? paperType = 0;
                        var details = res.vendor_po_detail;
                        if (details.FirstOrDefault() != null)
                        {
                            paperType = details.FirstOrDefault().client_po_detail.client_po.paper_type;
                        }
                        if (status.status == (byte)Status.Rejected)
                        {
                            foreach (var item in details)
                            {
                                var detail_ = _clientPORepository.Find(item.client_po_detail_id);
                                detail_.ordered_quantity = item.client_po_detail.ordered_quantity - item.ordered_quantity;
                                _clientPORepository.Update(detail_);
                            }
                        }
                        string name = paperType == 1 ? "Sharvi Traders Pvt Ltd" : "Sharvi Papers Pvt Ltd";
                        if (res.status_id == (byte)Status.NotStarted && status.status == (byte)Status.Approved)
                        {
                            var body = "Dear Sir," + Environment.NewLine + "Please find attached purchase order and confirm us by email." + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                                + "Thanks & Regards" + Environment.NewLine + Environment.NewLine + name + Environment.NewLine + "Plot No. 193, Street No.2," + Environment.NewLine +
                                "Krishna Colony, Opp. Sector-25," + Environment.NewLine + "Faridabad-121004 (Haryana)" + Environment.NewLine + "Website : www.sharvipapers.com";

                            var bodyClient = "Dear Sir," + Environment.NewLine + "Please find attached order confirmation and confirm us by email." + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                                + "Thanks & Regards" + Environment.NewLine + Environment.NewLine + name + Environment.NewLine + "Plot No. 193, Street No.2," + Environment.NewLine +
                                "Krishna Colony, Opp. Sector-25," + Environment.NewLine + "Faridabad-121004 (Haryana)" + Environment.NewLine + "Website : www.sharvipapers.com";


                            var path = System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/PO_Vendor_" + res.po_number.Replace("/", "-") + ".xlsx");
                            sendEmail(res.vendor.email, path, res.po_number, body, "PO_Vendor_" + res.po_number);

                            var client_po_number = res.vendor_po_detail.FirstOrDefault().client_po_detail.client_po.po_number;
                            var path1 = System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/" + res.id + "_OC_Client_" + client_po_number.Replace("/", "-") + ".xlsx");
                            sendEmail(res.client.email, path1, client_po_number, bodyClient, "OC_Client_" + client_po_number);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string createText = ex.Message + "from2";
                System.IO.File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/Excels/Logger.txt"), createText);

            }
        }
        public void sendEmail(string toEmail, string attachment, string number = "", string body = "", string Subject = "")
        {
            if (String.IsNullOrEmpty(Subject))
            {
                Subject = "Order Confirmation, Order Number: " + number;
            }
            Email email = new Email();
            email.Subject = Subject;
            email.Body = body;
            email.ToEmail = toEmail;
            email.Send(attachment);

        }
        #endregion


    }
}
