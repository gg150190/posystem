﻿using AutoMapper;
using POBusiness.Models;
using POData;
using POData.CommonRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository.Impl
{
    public class VendorRepository : IVendorRepository
    {
        private IPoRepository<vendor> _repository;
        private IPoRepository<state> _stateRepository;
        public VendorRepository(IPoRepository<vendor> repository, IPoRepository<state> stateRepository)
        {
            _repository = repository;
            _stateRepository = stateRepository;
        }
        public List<Vendor> All
        {
            get
            {
                var res = _repository.All.Where(t => t.is_active == true).OrderByDescending(t => t.id);
                return Mapper.Map<List<Vendor>>(res.ToList());
            }
        }

        public void Add(Vendor entity)
        {
            entity.is_active = true;
            var data = Mapper.Map<vendor>(entity);
            _repository.Add(data);
            entity.id = data.id;
        }

        public void Delete(object id)
        {
            var res = _repository.Find(id);
            res.is_active = false;
            _repository.Update(res);
        }

        public void Delete(Vendor entity)
        {
            throw new NotImplementedException();
        }

        public Vendor Find(object id)
        {
            var res = _repository.Find(id);
            return Mapper.Map<Vendor>(res);
        }

        public List<Vendor> FindBy(Expression<Func<Vendor, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Update(Vendor entity)
        {
            var data = Mapper.Map<vendor>(entity);
            var oldentry = _repository.Find(entity.id);
            Mapper.Map(entity, oldentry);
            _repository.Update(oldentry);
        }

        public List<States> GetStates()
        {
            var res = _stateRepository.All;
            return Mapper.Map<List<States>>(res.ToList());
        }

    }
}
