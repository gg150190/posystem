﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using POBusiness.Models;
using POData.CommonRepository;
using POData;
using AutoMapper;

namespace POBusiness.Repository.Impl
{
    public class ClientRepository : IClientRepository
    {
        private IPoRepository<client> _repository;
        private IPoRepository<state> _stateRepository;
        public ClientRepository(IPoRepository<client> repository, IPoRepository<state> stateRepository)
        {
            _repository = repository;
            _stateRepository = stateRepository;
        }
        public List<Client> All
        {
            get
            {
                var res = _repository.All.Where(t => t.is_active == true).OrderByDescending(t => t.id);
                return Mapper.Map<List<Client>>(res.ToList());
            }
        }

        public void Add(Client entity)
        {
            entity.is_active = true;
            var data = Mapper.Map<client>(entity);
            _repository.Add(data);
            entity.id = data.id;
        }

        public void Delete(object id)
        {
            var res = _repository.Find(id);
            res.is_active = false;
            _repository.Update(res);
        }

        public void Delete(Client entity)
        {
            throw new NotImplementedException();
        }

        public Client Find(object id)
        {
            throw new NotImplementedException();
        }

        public List<Client> FindBy(Expression<Func<Client, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Update(Client entity)
        {
            var data = Mapper.Map<client>(entity);
            var oldentry = _repository.Find(entity.id);
            Mapper.Map(entity, oldentry);
            _repository.Update(oldentry);
        }

        public List<States> GetStates()
        {
            var res = _stateRepository.All;
            return Mapper.Map<List<States>>(res.ToList());
        }
    }
}
