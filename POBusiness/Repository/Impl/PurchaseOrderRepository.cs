﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using POBusiness.Models;
using POData.CommonRepository;
using POData;
using POData.CommonRepository.Impl;
using AutoMapper;

namespace POBusiness.Repository.Impl
{
    public class PurchaseOrderRepository: IPurchaseOrderRepository
    {
        private IPoRepository<client_po> _repository;
        public PurchaseOrderRepository(IPoRepository<client_po> repository)
        {
            _repository = repository;
        }
        public List<PurchaseOrder> All
        {
            get
            {
                var res = _repository.All;
                return Mapper.Map<List<PurchaseOrder>>(res.ToList());
            }
        }

        public void Add(PurchaseOrder entity)
        {
            var data = Mapper.Map<client_po>(entity);
            _repository.Add(data);
        }

        public void Delete(object id)
        {
            _repository.Delete(id);
        }

        public void Delete(PurchaseOrder entity)
        {
            _repository.Delete(entity);
        }

        public PurchaseOrder Find(object id)
        {
            var res = _repository.Find(id);
            return Mapper.Map<PurchaseOrder>(res);
        }

        public List<PurchaseOrder> FindBy(Expression<Func<PurchaseOrder, bool>> predicate)
        {
            //Expression<Func<client_po, bool>> boolExpression = T => predicate.Compile()(T).GetValueOrDefault(false);
            //var res = _repository.FindBy(predicate);
            //return Mapper.Map<IQueryable<PurchaseOrder>>(res);
            return null;
        }
       
        public void Update(PurchaseOrder entity)
        {
            var data = Mapper.Map<client_po>(entity);
            _repository.Update(data);
        }
    }
}
