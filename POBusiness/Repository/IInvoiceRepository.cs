﻿using POBusiness.Models;
using POBusiness.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository
{
    public interface IInvoiceRepository : IRepository<Invoice>
    {
        List<Order> AllOrders { get; }
        Order FindByOrderId(object id);
        void RemoveCompleted(string deleteStr);
    }
}
