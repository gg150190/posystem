﻿using POBusiness.Models;
using POBusiness.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository
{
    public interface IClientRepository : IRepository<Client>
    {
       List<States> GetStates();
    }
}
