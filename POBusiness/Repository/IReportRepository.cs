﻿using POBusiness.Models;
using POBusiness.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository
{
    public interface IReportRepository
    {
        dynamic SearchReports(Report report);
    }
}
