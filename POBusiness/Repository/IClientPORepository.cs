﻿using POBusiness.Models;
using POBusiness.Repository.Common;
using POData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Repository
{
    public interface IClientPORepository : IRepository<ClientPO>
    {
        bool AddUpdateClientPODetail(ClientPODetail detail, bool isUpdate = false);
        bool DeleteClientPODetail(object id);
        List<RateType> AllRateTypes { get; }
        List<UnitType> AllUnitTypes { get; }
        bool DeleteClientPO_(object id);
        List<ClientPODetail> FindByClientId(int clientId, int paperTypeId, string po_number, int vendorId);
        void MarkAsComplete(int orderId);
        int CreateOrder(Order order);
        void UpdateOrder(OrderDetail detail);
        void UpdateRejectedOrder(OrderDetail detail);
        void DeleteRejectedOrder(OrderDetail detail);
        void CompleteRejectedOrder(int id, int type);
        List<Freight> GetFreights { get; }
        List<ClientPO> GetRejectedPOs();
        List<Order> GetRejectedOrders();
        List<Order> GetApprovedOrders();
        List<ClientPO> GetApprovedPO();
        List<string> GetAllPONumbers(int clientId);
        dynamic GetPendingDeliveries();
        void UpdateVendorPos(Int32 id);
        /// <summary>
        /// For Mobile API's
        /// </summary>
        dynamic GetDetailsCount();
        dynamic GetPODetails(int status, int total_records, int page_number);

        void UpdateStatus(StatusAppReject status);

    }
}
