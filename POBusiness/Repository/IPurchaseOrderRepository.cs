﻿using POBusiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POData;
using POData.CommonRepository;
using POBusiness.Repository.Common;

namespace POBusiness.Repository
{
    public interface IPurchaseOrderRepository : IRepository<PurchaseOrder>
    {


    }
}
