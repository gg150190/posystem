﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Enums
{
    enum Status
    {
        NotStarted = 1,
        Partial = 2,
        Completed = 3,
        Ordered = 4,
        Approved = 5,
        Rejected = 6,
        Updated = 7,
        RejectUpdated = 8,
        UpdateDeleted = 9
    }
}
