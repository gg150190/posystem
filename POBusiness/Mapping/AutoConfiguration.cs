﻿using AutoMapper;
using POBusiness.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBussiness.Mapping
{
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// Initialize all the automapper profiles.
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperModule>());
        }
        
    }
}
