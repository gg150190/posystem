﻿using AutoMapper;
using POBusiness.Enums;
using POBusiness.Models;
using POData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POBusiness.Mapping
{
    public class AutoMapperModule : Profile
    {
        public AutoMapperModule() : base()
        {
            CreateMap<client_po, ClientPO>()
                .ForMember(t => t.clientName, opt => opt.MapFrom(src => src.client.name))
                .ForMember(t => t.vendorName, opt => opt.MapFrom(src => src.vendor.name))
                .ForMember(t => t.createdByName, opt => opt.MapFrom(src => src.AspNetUser.Name))
                .ForMember(t => t.client_po_detail, opt => opt.MapFrom(src => src.client_po_detail.Where(t => t.is_deleted != true)))
                .ReverseMap();
            CreateMap<client_po_detail, ClientPODetail>()
                .ForMember(t => t.quantity_left, opt => opt.MapFrom(src => src.quantity - src.ordered_quantity))
                .ForMember(t => t.ordered_quantity, opt => opt.MapFrom(src => src.quantity - src.quantity))
                .ForMember(t => t.total_ordered_quantity, opt => opt.MapFrom(src => src.ordered_quantity))
                .ForMember(t => t.unitname, opt => opt.MapFrom(src => src.unit_type.unit_type1))
                .ForMember(t => t.rateType2, opt => opt.MapFrom(src => src.rate_type.rate_type1))
                .ForMember(t => t.size_name, opt => opt.MapFrom(src => src.size == "1" ? "inch" : "cm"))
                .ForMember(t => t.rim_weight, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(src.rim_weight), 2)))
                .ReverseMap();
            CreateMap<client, Client>().ReverseMap();
            CreateMap<vendor, Vendor>().ReverseMap();
            CreateMap<state, States>().ReverseMap();
            CreateMap<rate_type, RateType>().ReverseMap();
            CreateMap<unit_type, UnitType>().ReverseMap();
            CreateMap<freight, Freight>().ReverseMap();
            CreateMap<invoice_remarks, InvoiceRemarks>().ReverseMap();
            CreateMap<vendor_po, Order>()
                .ForMember(t => t.vendorname, opt => opt.MapFrom(src => src.vendor.name))
                .ForMember(t => t.clientname, opt => opt.MapFrom(src => src.client.name))
                .ForMember(t => t.freightName, opt => opt.MapFrom(src => src.freight.freight_type))
                .ForMember(t => t.createdByName, opt => opt.MapFrom(src => src.AspNetUser.Name))
                .ForMember(t => t.PaperType, opt => opt.MapFrom(src => src.vendor_po_detail.FirstOrDefault().client_po_detail.client_po.paper_type))
                .ForMember(t => t.pendingDeliveries, opt => opt.MapFrom(src => src.vendor_po_detail.Where(t => t.status_id != (byte)Status.Completed && Convert.ToDateTime(t.client_po_detail.delivery_date).Date < DateTime.Now.Date)))
                .ForMember(t => t.vendor_po_detail, opt => opt.MapFrom(src => src.vendor_po_detail.Where(t => t.is_deleted != true)))
                .ForMember(t => t.Buyer, opt => opt.MapFrom(src => src.client1))
                .ForMember(t => t.Consignee, opt => opt.MapFrom(src => src.client2))
                .ForMember(t => t.Delivery, opt => opt.MapFrom(src => src.client3))
                .ForMember(t => t.client_po_number, opt => opt.MapFrom(src => src.vendor_po_detail.FirstOrDefault().client_po_detail.client_po.po_number))
                .ReverseMap();
            CreateMap<vendor_po_detail, OrderDetail>()
                .ForMember(t => t.clientname, opt => opt.MapFrom(src => src.client_po_detail.client_po.client.name))
                .ForMember(t => t.vendorname, opt => opt.MapFrom(src => src.vendor_po.vendor.name))
                .ForMember(t => t.statusname, opt => opt.MapFrom(src => src.status.status1))
                .ForMember(t => t.paper_length, opt => opt.MapFrom(src => src.client_po_detail.paper_length))
                .ForMember(t => t.paper_breadth, opt => opt.MapFrom(src => src.client_po_detail.paper_breadth))
                .ForMember(t => t.bf_grain, opt => opt.MapFrom(src => src.client_po_detail.bf_grain))
                .ForMember(t => t.quality, opt => opt.MapFrom(src => src.client_po_detail.quality))
                .ForMember(t => t.gsm, opt => opt.MapFrom(src => src.client_po_detail.gsm))
                .ForMember(t => t.quantity, opt => opt.MapFrom(src => src.ordered_quantity))
                .ForMember(t => t.unitname, opt => opt.MapFrom(src => src.client_po_detail.unit_type.unit_type1))
                .ForMember(t => t.suppliedQuantitySum, opt => opt.MapFrom(src => src.invoice_remarks.Sum(t => Convert.ToDouble(t.supplied_quantity))))
                .ForMember(t => t.delivery_date, opt => opt.MapFrom(src => src.client_po_detail.delivery_date))
                .ForMember(t => t.PaperType, opt => opt.MapFrom(src => src.client_po_detail.client_po.paper_type))
                .ForMember(t => t.size_name, opt => opt.MapFrom(src => src.client_po_detail.size == "1" ? "inch" : "cm"))
                .ForMember(t => t.rateType2, opt => opt.MapFrom(src => src.client_po_detail.rate_type.rate_type1))
                .ForMember(t => t.rate, opt => opt.MapFrom(src => src.client_po_detail.rate))
                .ForMember(t => t.rim_weight, opt => opt.MapFrom(src => Math.Round(Convert.ToDecimal(src.client_po_detail.rim_weight), 2)))
                .ReverseMap();
        }
    }
}
